import { Component, ViewChild, OnInit } from '@angular/core';
import { EchartsDirective } from '../../../core/directives/echarts.directive';
import { SeguimientoEjecucionDataService } from '../../seguimiento-ejecucion-data.service'; 
import {ProcesarCompromisos } from '../../model/ProcesarCompromisos';

@Component({
  selector: 'app-se-sgc-compromisos',
  templateUrl: './se-sgc-compromisos.component.html',
  styleUrls: ['./se-sgc-compromisos.component.less']
})

export class SeSgcCompromisosComponent extends ProcesarCompromisos implements OnInit {

  @ViewChild(EchartsDirective) echart: EchartsDirective;
  public echartOption: any = {};
  
  setDataToChart(options) {
    this.echart.updateData(options);
  }

    goToProjects() {
    this.sData.goToProject(this.UEJ);
  }

  constructor(private sData : SeguimientoEjecucionDataService) {
    super();
    this.UEJ = "21-03-00";
    this.sData.dataGraficos.subscribe(data => {
      this.updateData(data);
    });

    this.sData.dataDatos.subscribe(entidades => {
      this.getDataEntidad(entidades)
    })

  }


  ngOnInit() {
  }

}
