/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CargarCdpHttpService } from './cargar-cdp-http.service';

describe('Service: CargarCdpHttp', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CargarCdpHttpService]
    });
  });

  it('should ...', inject([CargarCdpHttpService], (service: CargarCdpHttpService) => {
    expect(service).toBeTruthy();
  }));
});
