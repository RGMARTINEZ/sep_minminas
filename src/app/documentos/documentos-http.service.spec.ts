import { TestBed, inject } from '@angular/core/testing';
import { DocumentosHttpService } from './documentos-http.service';

describe('DocumentosHttpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DocumentosHttpService]
    });
  });

  it('should ...', inject([DocumentosHttpService], (service: DocumentosHttpService) => {
    expect(service).toBeTruthy();
  }));
});
