import { EchartsDirective } from '../../core/directives/echarts.directive';
import { AfterContentInit, Component, Input, ViewChild } from '@angular/core';

@Component({
  selector: 'app-grafico-general',
  templateUrl: './grafico-general.component.html',
  styleUrls: ['./grafico-general.component.css']
})
export class GraficoGeneralComponent implements AfterContentInit {
  @ViewChild(EchartsDirective) echart: EchartsDirective;
  @Input()
  set data(data: any) {
    let totales = {
      vigenteanterior: 0,
      vigenteactual: 0,
      cuotaaprobada: 0,
      solicitado: 0
    };

    for (let i = 0; i < data.length; i++) {
      totales.vigenteanterior += data[i].vigenteanterior;
      totales.vigenteactual += data[i].vigenteactual;
      totales.cuotaaprobada += data[i].cuotaaprobada;
      totales.solicitado += data[i].solicitado;
    }

    this.echartOption.series[0].data = [totales.vigenteanterior, totales.vigenteactual, totales.solicitado, totales.cuotaaprobada];
    if (this.echart.isinit) {
          this.echart.updateData(this.echartOption);
    }
  };

    public echartOption: any = {
        title : {
            show: false
        },
        grid: {
          x : 50,
          y : 50,
          x2: 50,
          y2: 50
        },
        tooltip : {
            trigger: 'axis'
        },
        legend: {
            show: false
        },
        toolbox: {
            show : false,
        },
        xAxis : [
            {
                type : 'category',
                data : ['2016', '2017', 'S. 2018', 'A. 2018']
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name: '',
                type: 'bar',
                data: [0, 0, 0, 0],
                  itemStyle: {
                  normal: {
                    color: '#069169'
                  }
                }
            }
        ]
    };

  constructor() {

  }

    ngAfterContentInit() {
      if (this.echart.isinit) {
        this.echart.updateData(this.echartOption);
      }
    }


}
