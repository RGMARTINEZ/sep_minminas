import { Component, OnInit } from '@angular/core';
import { ProyectoHttpService } from '../proyectos/proyecto-http.service';
import { TagsHttpService } from '../tags/tags-http.service';
import { Tag } from '../core/models/Tag';
import { MessageService } from '../core/services/message.service'; 

@Component({
  selector: 'app-ranking-entidades',
  templateUrl: './ranking-entidades.component.html',
  styleUrls: ['./ranking-entidades.component.less'],
  providers : [ProyectoHttpService, TagsHttpService ]
})
export class RankingEntidadesComponent implements OnInit {


  data : Array<any> = new Array<any>();
  semana : number
  mes : number
  anio : number = new Date ().getFullYear()
  tipoPresupuesto : string

  noData : boolean = true
  loading : boolean = false
  error : boolean = false
  noSIIFLoader : boolean = false

 tag = {
      items: [],
      dataAutocomplete: [],
      data: [],
      errorMessage: "",
      setItems: function (tags) {
        for (var i = 0; i < tags.length; i++) {
          this.items.push(tags[i].nombreTag);
        }
      },
      setTags: function (tags) {
        this.data = tags;
        for (var i = 0; i < tags.length; i++) {
          this.dataAutocomplete.push(tags[i].nombreTag);
        }
      },
      getTags: function () {
        var result = [];
        for (var i = 0; i < this.items.length; i++) {
          var tmpTag = new Tag(null, this.items[i]);

          for (var j = 0; j < this.data.length; j++) {
            if (this.data[j].nombreTag == this.items[i]) {
              tmpTag.idTag = this.data[j].idTag;
              tmpTag.nombreTag = this.data[j].nombreTag;
            }
          }
          result.push(tmpTag);
        }
        return result;
      }
    }

    getTags() {
      this.tagsHttpService.getTags()
        .subscribe(tags => this.tag.setTags(tags),
        error => this.tag.errorMessage = <any>error);
    }


  constructor(private proyectoHttpService: ProyectoHttpService, 
  private tagsHttpService : TagsHttpService,
  private messageService : MessageService) { }

  procesarDatos(entidades) {
    this.loading = false;
    this.noData = (entidades.lenght == 0) ? true : false;
    this.data = [];
    var todosNull = 0;
    for (var i = 0; i < entidades.length; i++) {
      if (entidades[i].nombreuej != '') {
      entidades[i].percent = (entidades[i].obligacion == null || entidades[i].obligado == null) ? 0 : entidades[i].obligado / entidades[i].obligacion  * 100;
      this.data.push(entidades[i]);
      (entidades[i].obligado == null) ? todosNull += 1 : null;
      }
    }
    this.noSIIFLoader = (todosNull == entidades.length - 1) ? true : false;
  }

  setError(error) {
    this.messageService.showError(null, "Ocurrió un error al intentar obtener el ranking.")
    this.error = true;
    this.noData = false;
    this.loading = false;
  }


  obtenerDatos() {
    if (this.anio != null && this.mes != null && this.tipoPresupuesto != null) {
      this.loading = true;
      this.noData = false;
      this.error = false;
      this.noSIIFLoader = false;
      this.data = [];      
      this.proyectoHttpService.getRankingEntidades(this.anio, this.mes, this.tipoPresupuesto, this.tag.getTags())
        .subscribe(entidades => this.procesarDatos(entidades),
        error => this.setError(error));
      
    }
  }

  ngOnInit() {
    this.getTags();
  }

}
