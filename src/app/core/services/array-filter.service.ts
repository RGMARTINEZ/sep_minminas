import { Injectable } from '@angular/core';
import { ParamFilter } from '../models/paramFilter';

@Injectable()
export class ArrayFilterService {

  constructor() { }

  isValidItem(e, itemArray) {
    var result = false;
    if (e.key.charAt(0) == '$') {
      for (var objKey in itemArray) {
        if (e.precision) {
          if (itemArray[objKey] == e.value) {
            result = true;
            break;
          }
        } else {
          var text = ('' + itemArray[objKey]).toLowerCase();
          if (('' + text).toLowerCase().indexOf(('' + e.value).toLowerCase()) !== -1) {
            result = true;
            break;
          }
        }
      }
    } else if ( Array.isArray(e.value)) {
      var text = ('' + itemArray[e.key]).toLowerCase();
      for (var i = 0; i < e.value.length; i++) {
        if (e.precision) {
          if (itemArray[e.key] == e.value[i]) {
            result = true;
            break;
          }
        } else {
          if (('' + text).toLowerCase().indexOf(('' + e.value[i]).toLowerCase()) !== -1) {
            result = true;
            break;
          }
        }
      }
    } else {
      if (e.precision) {
        if (itemArray[e.key] == e.value) {
          result = true;
        }
      } else {
        var text = ('' + itemArray[e.key]).toLowerCase();
        if (('' + text).toLowerCase().indexOf(('' + e.value).toLowerCase()) !== -1) {
          result = true;
        }
      }
    }
    return result;
  }



  filter(array : Array<any>, expression : Array<ParamFilter>) {
    var result = [];
    var isValid = true;

      if (Array.isArray(array)) {
        for (var i = 0; i < array.length; i++) {
          isValid = true;
          for (var e in expression) {
            (expression[e].value !== undefined && expression[e].value !== '' && isValid) ? 
              isValid = this.isValidItem(expression[e], array[i]): null;
          }
          (isValid) ? result.push(array[i]): null;
        }
      }
      return result;
  }

}
