import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProyectoHttpService } from '../proyecto-http.service';
import { ProyectoDataService } from '../proyecto-data.service';
import { Proyecto } from '../../core/models/Proyecto';
import { PaginationList } from '../../core/models/paginationsList';


@Component({
  selector: 'app-proyecto-list',
  templateUrl: './proyecto-list.component.html',
  styleUrls: ['./proyecto-list.component.less'],
  providers: [ProyectoHttpService]
})

export class ProyectoListComponent extends PaginationList implements OnInit {

  public onSelectProyecto(proyecto: Proyecto) {
    this.ProyectoDataService.setData(proyecto);
    this.router.navigate(['app/proyectos/proyecto']);
  }

  constructor(private proyectoHttpService: ProyectoHttpService,
    private ProyectoDataService: ProyectoDataService,
    private router: Router) {
    super();
    this.paginations.itemsPerPage = 10;
  }



  public listaProyectos() {
    this.loading = true;
    this.error = false;
    this.noData = false;
    this.proyectoHttpService.getListaProyectos()
      .subscribe(proyectos => this.setData(proyectos),
      error => this.setError(error));
  }


  ngOnInit() {
    this.listaProyectos();
  }

}
