import { ArrayFilterService } from '../services/array-filter.service';
import { ParamFilter } from '../models/paramFilter';

export class PaginationList {


    constructor() {
    }

    paginations: any = {
        maxSize: 3,
        itemsPerPage: 10,
        currentPage: 0,
        totalItems: 0
    };

    data: Array<any> = new Array<any>()
    dataSource: Array<any> = new Array<any>()
    selectedItems: Array<any> = new Array<any>()
    noData: boolean = true
    loading: boolean = false
    error: boolean = false
    filterText: string = ""
    paramsFilters : Array<ParamFilter> = new Array<ParamFilter>()
    arrayFilterService: ArrayFilterService = new ArrayFilterService();

    filter() {
        let paramFilter: ParamFilter = new ParamFilter();
        paramFilter.value = this.filterText;
        this.paramsFilters.push(paramFilter);
        this.selectedItems = this.arrayFilterService.filter(this.dataSource, this.paramsFilters);
        this.onAfterFilterSelectedItems();
        this.paginations.totalItems = this.selectedItems.length;
        this.paginations.currentPage = 1;
        this.changePage({ page : this.paginations.currentPage });
    }

    public changePage(param) {
        const firstItem = (param.page === 1) ? 0 :
            (param.page * this.paginations.itemsPerPage) -
            this.paginations.itemsPerPage;

        this.data = this.selectedItems.slice(firstItem, param.page *
            this.paginations.itemsPerPage);

            this.onChangedData();

    }

    public setData(data) {
        this.selectedItems = data;
        this.dataSource = data;
        this.paginations.totalItems = this.selectedItems.length;
        this.paginations.currentPage = 1;
        this.changePage({ page : this.paginations.currentPage });
        this.loading = false;
        this.noData = false;
        (this.dataSource.length < 1) ? this.noData = true : null;
    }

    public setError(error: any) {
        this.noData = false
        this.loading = false
        this.error = true
    }


    public onChangedData() {
    }

    public onAfterFilterSelectedItems() {

    }

}