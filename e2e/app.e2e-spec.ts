import { AlBasePage } from './app.po';

describe('al-base App', function() {
  let page: AlBasePage;

  beforeEach(() => {
    page = new AlBasePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
