import { Component, OnInit } from '@angular/core';
import { CargarCdpHttpService } from '../cargar-cdp-http.service';
import { LoadingService } from '../../core/loading/loading.service';

@Component({
  selector: 'app-cs-list',
  templateUrl: './cs-list.component.html',
  styleUrls: ['./cs-list.component.less'],
  providers : [CargarCdpHttpService]
})
export class CsListComponent implements OnInit {

  public data: any = [];
  errorMessage: any;

  public setData(data: any) {
    this.data = data;
    this.loading.setLoading(false);
  }

  public setError(error: any)  {
    this.errorMessage = error;
    this.loading.setLoading(false);
  }

  constructor(private cargarCdpHttpService: CargarCdpHttpService, private loading: LoadingService) { }

  public listaSiif() {
    this.loading.setLoading(true);
    this.cargarCdpHttpService.getListSiif()
      .subscribe(
        siif => this.setData(siif),
        error => this.setError(error));
  }


  ngOnInit() {
    this.listaSiif();
  }

}
