import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComportamientoEntidadesComponent } from './comportamiento-entidades.component';

describe('ComportamientoEntidadesComponent', () => {
  let component: ComportamientoEntidadesComponent;
  let fixture: ComponentFixture<ComportamientoEntidadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComportamientoEntidadesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComportamientoEntidadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
