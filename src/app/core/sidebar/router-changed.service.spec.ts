import { TestBed, inject } from '@angular/core/testing';
import { RouterChangedService } from './router-changed.service';

describe('RouterChangedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RouterChangedService]
    });
  });

  it('should ...', inject([RouterChangedService], (service: RouterChangedService) => {
    expect(service).toBeTruthy();
  }));
});
