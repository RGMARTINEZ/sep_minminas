import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SeguimientoEjecucionDataService } from './seguimiento-ejecucion-data.service';
import { ProyectoHttpService } from '../proyectos/proyecto-http.service';
import { TagsHttpService } from '../tags/tags-http.service';
import { Tag } from '../core/models/Tag';
import { LoadingService } from '../core/loading/loading.service';
import { MessageService } from '../core/services/message.service';

import { StateParamService } from '../core/services/state-param.service';
import { STATE_PARAM_GLOBAL } from '../globals';
import { GlobalVariablesService } from '../core/services/global-variables.service';
import { RouterChangedService } from '../core/sidebar/router-changed.service';

@Component({
  selector: 'app-seguimiento-ejecucion',
  templateUrl: './seguimiento-ejecucion.component.html',
  styleUrls: ['./seguimiento-ejecucion.component.less'],
  providers: [SeguimientoEjecucionDataService, ProyectoHttpService, TagsHttpService]
})
export class SeguimientoEjecucionComponent implements OnInit {

  currentOptionsConfig = {
    labelField: 'nombreTag',
    valueField: 'nombreTag',
    searchField: ['nombreTag', 'idTag'],
    maxItems: 10,
    highlight: false,
    create: false,
    persist: true,
    plugins: ['dropdown_direction', 'remove_button'],
    dropdownDirection: 'down'
  };
  loading: boolean;

  currentMes: number = null;
  currentEntidad: string = null;
  anio: number = new Date ().getFullYear();
  tipoPresupuesto : string = "TODOS";
  result : any;
  isLoadingInitial : boolean = false;
  entidades_hidden = [];

  tag = {
    items: [],
    dataAutocomplete: [],
    data: [],
    errorMessage: "",
    setItems: function (tags) {
      for (var i = 0; i < tags.length; i++) {
        this.items.push(tags[i].nombreTag);
      }
    },
    getTags: function () {
      var result = [];
      for (var i = 0; i < this.items.length; i++) {
        var tmpTag = new Tag(null, this.items[i]);

        for (var j = 0; j < this.data.length; j++) {
          if (this.data[j].nombreTag == this.items[i]) {
            tmpTag.idTag = this.data[j].idTag;
            tmpTag.nombreTag = this.data[j].nombreTag;
          }
        }
        result.push(tmpTag);
      }
      return result;
    }
  }

    getTags() {
      this.tagsHttpService.getTags()
        .subscribe(tags => this.setTags(tags),
        error => this.tag.errorMessage = <any>error);
    }

    setTags(tags) {
    this.tag.data = tags;
      for (var i = 0; i < tags.length; i++) {
        this.tag.dataAutocomplete.push(tags[i]);
      }
      (this.isLoadingInitial == true) ? this.cargaInicial() : null;
    }

    changedEntidad() {
      this.seguimientoEjecucionDataService.setShowEntidad(this.currentEntidad);
      this.updateData();
    }

  setError(error) {
    this.loadingService.setLoading(false);
    this.messageService.showError(null, "Ocurrió un error al intentar obtener los datos.")
  }

  setData(data) {
    this.loadingService.setLoading(false);
    this.entidades_hidden = [];
    for (var i = 0; i < data.length ; i++) {
      data[i].mes = Number(this.currentMes);
      var entidad_hidden = true;
      for (var k = 0; k < data[i].apropiacionVigente.length ; k++) {
        if (data[i].apropiacionVigente[k].valor != 0) {
          if (this.currentEntidad == "TODOS" || this.currentEntidad == data[i].uej) {
            entidad_hidden = false;
            break;
          }
        }
      }
      (entidad_hidden == true) ? this.entidades_hidden.push(data[i].uej) :null;
    }
    this.seguimientoEjecucionDataService.setDataGraficos(data);

  }

  procesarDatos(entidades) {
    var param = {
      tipoPresupuesto : this.tipoPresupuesto,
      mes : this.currentMes,
      entidades : entidades
    }
    this.seguimientoEjecucionDataService.setDataDatos(param);
  }



  public updateData() {
    if (this.currentMes == null || this.currentEntidad == null) {
//      this.messageService.showInfo(null, "Debe seleccionar un mes");
    } else {

        var _param = {
          entidad: this.currentEntidad,
          mes: this.currentMes,
          tipoPresupuesto : this.tipoPresupuesto,
          tags : this.tag.items
        }
        this.stateParamService.setParams( STATE_PARAM_GLOBAL , _param );
        this.loadingService.setLoading(true);
        this.proyectoHttpService.getLiquidacionAcumulada( this.anio, this.currentMes, this.tipoPresupuesto, this.tag.getTags() )
          .subscribe(data => this.setData(data),
          error => this.setError(error));

        this.proyectoHttpService.getLiquidacionAcumuladaWithPorcentajes(this.anio, this.currentMes, this.tipoPresupuesto, this.tag.getTags())
        .subscribe(entidades => this.procesarDatos(entidades),
        error => this.setError(error));
    }
  }

  cargaInicial() {
    this.isLoadingInitial = false;
    this.loadingService.setLoading(false);
    this.result = this.stateParamService.getParams(STATE_PARAM_GLOBAL)
    if (this.result != null) {
        this.currentEntidad = this.result.entidad;
        this.currentMes = this.result.mes;
        this.tipoPresupuesto = this.result.tipoPresupuesto;
        this.tag.items = (this.result.tags != null) ? this.result.tags : [];
        if (this.currentEntidad != null && this.currentMes != null && this.tipoPresupuesto != null) {
          this.changedEntidad();
          this.updateData();
        }
    }
  }


  constructor( private route: ActivatedRoute, private seguimientoEjecucionDataService: SeguimientoEjecucionDataService,
    private proyectoHttpService: ProyectoHttpService,
    private tagsHttpService: TagsHttpService,
    private loadingService: LoadingService,
    private messageService : MessageService,
    private stateParamService: StateParamService,
    private router : Router,
    private globalVariablesService : GlobalVariablesService,
    private routerChangedService: RouterChangedService) {
  }

  ngOnInit() {
    this.loadingService.setLoading(true);
    this.isLoadingInitial = true;
    this.getTags();
    this.currentEntidad = "TODOS";
    this.changedEntidad();

    this.routerChangedService.onChanged.subscribe(url => {
      (url == '/app/seguimiento') ? this.cargaInicial() : null;
    })

    this.seguimientoEjecucionDataService.goProject.subscribe(uej => {
      this.currentEntidad = uej;
        this.router.navigate(['app/seguimientoProyecto']);
    })
  }


}
