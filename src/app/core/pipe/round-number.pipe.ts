import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'roundNumber'
})
export class RoundNumberPipe implements PipeTransform {

  transform(value: any, convert: any): any {
    var filters = {"thousandsOfmillons": 1000000000 , "millons": 1000000, "thousands": 1000, "hundreds" : 100  };
    var _return = value; 

    if (filters[convert] != undefined) {
      _return = value / filters[convert]
    }
    return _return;
  }

}
