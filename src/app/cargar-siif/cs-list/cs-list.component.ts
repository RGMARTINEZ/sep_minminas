import { Component, OnInit } from '@angular/core';
import { CargarSiifHttpService } from '../cargar-siif-http.service';
import { LoadingService } from '../../core/loading/loading.service';

@Component({
  selector: 'app-cs-list',
  templateUrl: './cs-list.component.html',
  styleUrls: ['./cs-list.component.less'],
  providers : [CargarSiifHttpService]
})
export class CsListComponent implements OnInit {

  public data: any = [];
  errorMessage: any;

  public setData(data: any) {
    this.data = data;
    this.loading.setLoading(false);
  }

  public setError(error: any)  {
    this.errorMessage = error;
    this.loading.setLoading(false);
  }

  constructor(private cargarSiifHttpService: CargarSiifHttpService, private loading: LoadingService) { }

  public listaSiif() {
    this.loading.setLoading(true);
    this.cargarSiifHttpService.getListSiif()
      .subscribe(
        siif => this.setData(siif),
        error => this.setError(error));
  }


  ngOnInit() {
    this.listaSiif();
  }

}
