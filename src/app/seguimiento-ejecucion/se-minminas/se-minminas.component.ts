import { Component, OnInit, Input } from '@angular/core';
import { SeguimientoEjecucionDataService } from '../seguimiento-ejecucion-data.service';

@Component({
  selector: 'app-se-minminas',
  templateUrl: './se-minminas.component.html',
  styleUrls: ['./se-minminas.component.less']
})
export class SeMinminasComponent implements OnInit {
  @Input()
  set entidadHidden(entidades : any) {
    (entidades.length == 0) ? this.visible = true: null;
    for (var i = 0; i < entidades.length; i++) {
      (entidades[i] == "21-01-01") ? this.visible = false: null; 
    }
  }

  visible : boolean;

  ngOnInit() {
  }

  isVisible(entidad) {
    this.visible = (entidad == "21-01-01" || entidad == "TODOS") ? true : false;
  }


    constructor(private sData : SeguimientoEjecucionDataService) {
    this.sData.showEntidad.subscribe(entidad => {
      this.isVisible(entidad);
    });
  }

}
