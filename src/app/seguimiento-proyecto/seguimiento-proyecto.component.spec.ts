import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeguimientoProyectoComponent } from './seguimiento-proyecto.component';

describe('SeguimientoProyectoComponent', () => {
  let component: SeguimientoProyectoComponent;
  let fixture: ComponentFixture<SeguimientoProyectoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeguimientoProyectoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeguimientoProyectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
