import { Component, OnInit } from '@angular/core';
import { SectorHttpService } from '../sector-http.service';
import { SectorDataService } from '../sector-data.service';

@Component({
  selector: 'app-resumen-cifras',
  templateUrl: './resumen-cifras.component.html',
  styleUrls: ['./resumen-cifras.component.css'],
  providers: [SectorHttpService]
})
export class ResumenCifrasComponent implements OnInit {

  mes : number = 2;
  anio : number = 2017;
  data : Array<any> = new Array<any>();
  
  loading : boolean = false;
  noData : boolean = true;
  error : boolean = false;
  noSIIFLoader : boolean = false;


  funcionamiento = {
    apropiacion_disponible : 0,
    comprometido : 0,
    obligado: 0
  };

  inversion = {
    apropiacion_disponible : 0,
    comprometido : 0,
    obligado: 0
  };

  total = {
    apropiacion_disponible : 0,
    comprometido : 0,
    obligado: 0
  }
  
  constructor(private sectorHttpService: SectorHttpService, private sectorDataService : SectorDataService) {

  }
  

  procesarData(data) {
      this.loading = false;
      this.noData = (data.length == 0) ? true : false;
      this.error = false;
      this.noSIIFLoader = false;

      this.total.apropiacion_disponible = 0;
      this.total.comprometido = 0;
      this.total.obligado = 0;

    for (var i = 0; i < data.length ; i++) {
      if (data[i].descripcion == "FUNCIONAMIENTO") {
        this.funcionamiento = data[i];
      } else if (data[i].descripcion == "INVERSION") {
        this.inversion = data[i];
      }
      this.total.apropiacion_disponible += data[i].apropiacion_disponible;
      this.total.comprometido += (data[i].comprometido == null) ? 0: data[i].comprometido;
      this.total.obligado += (data[i].obligado == null) ? 0 : data[i].obligado;
    }

    var funcionamiento_percent =  100 - this.funcionamiento.comprometido / this.funcionamiento.apropiacion_disponible * 100;
    var inversion_percent =  100 - this.inversion.comprometido / this.inversion.apropiacion_disponible * 100;
    this.noSIIFLoader = (this.total.obligado == 0) ? true : false;
   }

  setError(error) {
      this.loading = false;
      this.noData = false;
      this.error = true;
      this.noSIIFLoader = false;

  }

  obtenerDatos(data) {
    this.anio = data.anio;
    this.mes = data.mes;

  this.loading = true;
  this.noData = false;
  this.error = false;
  this.noSIIFLoader = false;

    if (this.mes != null && this.anio != null) {
      this.sectorHttpService.getLiquidacionSectorInvFun(this.anio, this.mes)
        .subscribe(data => this.procesarData(data),
        error => this.setError(error));
    }
  }

  ngOnInit() {
    (this.sectorDataService._data !== null) ? this.obtenerDatos(this.sectorDataService._data) : null;
    this.sectorDataService.data.subscribe(data => {
      this.obtenerDatos(data);
    });
    }

}
