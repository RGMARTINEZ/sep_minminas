import { Directive, ElementRef,  Renderer, AfterViewInit } from '@angular/core';
declare var jQuery:any;

@Directive({
  selector: '[appAlSelect]'
})
export class AlSelectDirective {

  constructor(private el: ElementRef, private renderer: Renderer) {
   }

   ngAfterViewInit() {
        jQuery(this.el.nativeElement).material_select();
    }

}
