import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpService } from '../core/services/http.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DocumentosHttpService extends HttpService {


  constructor (http: Http) {
    super(http);
  }

  public END_POINT = '';
  getListaDocumentos(): Observable<any> {
    return this.get('Normatividad');
  }

 getListaDocumentosByFilter(anio: number, mes: number): Observable<any> {
   return this.get('Normatividad/findByFechaAndMes?anio=' + anio +"&mes=" + mes);
 }

 getFile(adjuntoid: string) {
  return this.get('Normatividad/files/' + adjuntoid);
 }

 saveNormatividad(documentoNormatividad: any): Observable<any> {
  return this.post('Normatividad', documentoNormatividad);
}

}
