import { TestBed, inject } from '@angular/core/testing';

import { SectorHttpService } from './sector-http.service';

describe('SectorHttpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SectorHttpService]
    });
  });

  it('should ...', inject([SectorHttpService], (service: SectorHttpService) => {
    expect(service).toBeTruthy();
  }));
});
