export class Obligacion {
    
    idObligacion: number;
    valor: number;
    mes: number;
    valorEnAutorizacion: number;
    usuarioSolicita: string;
    fechaSolicitud: string;
    idActividad: number;

} 