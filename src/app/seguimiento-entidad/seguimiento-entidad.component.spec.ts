import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeguimientoEntidadComponent } from './seguimiento-entidad.component';

describe('SeguimientoEntidadComponent', () => {
  let component: SeguimientoEntidadComponent;
  let fixture: ComponentFixture<SeguimientoEntidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeguimientoEntidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeguimientoEntidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
