import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { ProyectoHttpService } from '../proyectos/proyecto-http.service';
import { PaginationList } from '../core/models/paginationsList';
import { TagsHttpService } from '../tags/tags-http.service';
import { Tag } from '../core/models/Tag';
import { MessageService } from '../core/services/message.service';
import { LoadingService } from '../core/loading/loading.service';
import { StateParamService } from '../core/services/state-param.service';
import { STATE_PARAM_GLOBAL } from '../globals';
import { GlobalVariablesService } from '../core/services/global-variables.service';

@Component({
  selector: 'app-seguimiento-entidad',
  templateUrl: './seguimiento-entidad.component.html',
  styleUrls: ['./seguimiento-entidad.component.css'],
  providers : [ProyectoHttpService, TagsHttpService]
})

export class SeguimientoEntidadComponent extends PaginationList implements OnInit {

  currentOptionsConfig = {
    labelField: 'nombreTag',
    valueField: 'nombreTag',
    searchField: ['nombreTag', 'idTag'],
    maxItems: 10,
    highlight: false,
    create: false,
    persist: true,
    plugins: ['dropdown_direction', 'remove_button'],
    dropdownDirection: 'down'
  };



semana : number
  mes : number
  anio : number = new Date ().getFullYear();
  tipoPresupuesto : string
  showData : boolean = false;
  sub : any;
  ordenEntidades : any = ["MME", "ANH","ANM", "CREG","IPSE", "SGC","UPME"];
  result : any;


public onChangedData() {
  this.calcularTotales(this.selectedItems);
}


  totales : any = {
    apropiacion_disponible : 0,
    compromiso : 0,
    por_comprometer : 0,
    comprometido : 0,
    obligacion : 0,
    obligado : 0,
    por_obligar_de_los_compromiso : 0,
    comprometido_apropiacion : 0,
    compromiso_apropiacion: 0,
    obligado_apropiacion: 0,
    obligacion_apropiacion: 0
  };


    tag = {
      config: {
        create: true,
        valueField: 'idTag',
        labelField: 'nombreTag',
        delimiter: '|',
        placeholder: 'Pick something',
        onInitialize: function(selectize){
        },
      },
      items: [],
      dataAutocomplete: [],
      data: [],
      errorMessage: '',
      setItems: function (tags) {
        for (let i = 0; i < tags.length; i++) {
          this.items.push(tags[i].nombreTag);
        }
      },
      setTags: function (tags) {
        this.data = tags;
        for (let i = 0; i < tags.length; i++) {
          this.dataAutocomplete.push(tags[i]);
        }
      },
      getTags: function () {
        const result = [];
        for (let i = 0; i < this.items.length; i++) {
          let tmpTag = new Tag(null, this.items[i]);

          for (let j = 0; j < this.data.length; j++) {
            if (this.data[j].nombreTag == this.items[i]) {
              tmpTag.idTag = this.data[j].idTag;
              tmpTag.nombreTag = this.data[j].nombreTag;
            }
          }
          result.push(tmpTag);
        }
        return result;
      }
    }

    getTags() {
      this.tagsHttpService.getTags()
        .subscribe(tags => this.tag.setTags(tags),
        error => this.tag.errorMessage = <any>error);
    }

    goToSeguimiento(uej) {
      this.result.entidad = uej;
      this.router.navigate(['app/seguimiento']);
    }

    showDetalle() {
      this.showData = !this.showData;
    }

    calcularTotales(proyectos) {
        this.totales.apropiacion_disponible = 0;
        this.totales.compromiso = 0;
        this.totales.por_comprometer = 0;
        this.totales.comprometido = 0;
        this.totales.obligacion = 0;
        this.totales.obligado = 0;
        this.totales.por_obligar_de_los_compromiso = 0;
          for (var i = 0; i < proyectos.length ; i++) {
            if (proyectos[i].nombreuej != "") {
                this.totales.apropiacion_disponible += proyectos[i].apropiacion_disponible;
                this.totales.compromiso += proyectos[i].compromiso;
                this.totales.por_comprometer += proyectos[i].por_comprometer;
                this.totales.comprometido += proyectos[i].comprometido;
                this.totales.obligacion += proyectos[i].obligacion;
                this.totales.obligado += proyectos[i].obligado;
                this.totales.por_obligar_de_los_compromiso += proyectos[i].por_obligar_de_los_compromiso;
            }
          }
          this.totales["comprometido_apropiacion"] = this.totales.comprometido / this.totales.apropiacion_disponible * 100;
          this.totales["compromiso_apropiacion"] = this.totales.compromiso / this.totales.apropiacion_disponible * 100;
          this.totales["obligado_apropiacion"] = this.totales.obligado / this.totales.apropiacion_disponible * 100;
          this.totales["obligacion_apropiacion"] = this.totales.obligacion / this.totales.apropiacion_disponible * 100;
    }



  procesarData(proyectos) {
    this.loadingPage.setLoading(false);
    var result = [];
    for (var i = 0; i < proyectos.length ; i++) {
      if (proyectos[i].nombreuej != "") {
        proyectos[i]["comprometido_apropiacion"] = proyectos[i].comprometido / proyectos[i].apropiacion_disponible * 100;
        proyectos[i]["compromiso_apropiacion"] = proyectos[i].compromiso / proyectos[i].apropiacion_disponible * 100;
        proyectos[i]["obligado_apropiacion"] = proyectos[i].obligado / proyectos[i].apropiacion_disponible * 100;
        proyectos[i]["obligacion_apropiacion"] = proyectos[i].obligacion / proyectos[i].apropiacion_disponible * 100;
        proyectos[i]["orden"] = this.ordenEntidades.indexOf(proyectos[i].abreviacion);
        result.push(proyectos[i]);
      }
    }
    this.calcularTotales(result);
    this.setData(result);
  }

  setError(error) {
    this.error = true;
    this.noData = false;
    this.loading = false;
    this.loadingPage.setLoading(false);
    this.message.showError(null, "Ocurrió un error al intentar obtener los datos.")
  }


  obtenerDatos() {
    if (this.mes == null || this.tipoPresupuesto == null) {
      this.message.showWarning(null,"Debe seleccionar mes y tipoPresupuesto")
    } else {


      var _param = {
            entidad: this.result.entidad,
            mes: this.mes,
            tipoPresupuesto : this.tipoPresupuesto,
            tags : this.tag.items
      }
      this.stateParamService.setParams( STATE_PARAM_GLOBAL , _param );

      this.loading = true;
      this.noData = false;
      this.error = false;
      this.data = [];
      this.loadingPage.setLoading(true);
      this.proyectoHttpService.getLiquidacionAcumuladaWithPorcentajes(this.anio, this.mes, this.tipoPresupuesto, this.tag.getTags())
        .subscribe(proyectos => this.procesarData(proyectos),
        error => this.setError(error));
    }
  }


    constructor(private proyectoHttpService: ProyectoHttpService,
      private tagsHttpService: TagsHttpService,
      private message : MessageService,
      private loadingPage : LoadingService,
      private router : Router,
      private route: ActivatedRoute,
      private stateParamService: StateParamService,
      private globalVariablesService : GlobalVariablesService) {

    super();
    this.paginations.itemsPerPage = 15;

   }

  ngOnInit() {
    this.semana = 5;
    this.getTags();
    this.result = this.stateParamService.getParams(STATE_PARAM_GLOBAL)
    if (this.result != null) {
      this.mes = this.result.mes;
      this.tipoPresupuesto = this.result.tipoPresupuesto;
      (this.result.tags != null) ? this.tag.items = this.result.tags : null;
      if (this.mes != null && this.tipoPresupuesto != null) {
        this.obtenerDatos();
      }
    }

  }


}
