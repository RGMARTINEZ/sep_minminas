import { Component, OnInit } from '@angular/core';
import { SectorHttpService } from '../sector-http.service';
import { SectorDataService } from '../sector-data.service';

@Component({
  selector: 'app-variables-relevantes',
  templateUrl: './variables-relevantes.component.html',
  styleUrls: ['./variables-relevantes.component.css'],
  providers : [SectorHttpService]
})
export class VariablesRelevantesComponent implements OnInit {

  mes : number = 1;
  anio : number = 2017;
  data : Array<any> = new Array<any>();

  loading : boolean = false;
  noData : boolean = true;
  error : boolean = false;
  noSIIFLoader : boolean = false;

  total = {
    apropiacion_disponible : 0,
    comprometido : 0,
    obligado: 0
  }

  sinComprometer = {
    funcionamiento : 0,
    inversion : 0
  }
  
  constructor(private sectorHttpService: SectorHttpService, private sectorDataService : SectorDataService) {}

  procesarData(data) {
        this.error = false;
        this.noData = (data.lenght == 0) ? true : false;
    this.loading = false;
    this.noSIIFLoader = false;

    this.total.apropiacion_disponible = 0;
    this.total.comprometido = 0;
    this.total.obligado = 0;


    var funcionamientoApropiacion, funcionamiento_percent, inversionApropiacion, inversion_percent =  0;

    for (var i = 0; i < data.length; i++) {
      if (data[i].descripcion == "FUNCIONAMIENTO") {
        funcionamientoApropiacion = data[i].apropiacion_disponible;
         funcionamiento_percent =  100 - (data[i].comprometido / data[i].apropiacion_disponible * 100);
      } else if (data[i].descripcion == "INVERSION") {
        inversionApropiacion = data[i].apropiacion_disponible;
         inversion_percent =  100 - (data[i].comprometido / data[i].apropiacion_disponible * 100) ;
      }
      this.total.apropiacion_disponible += data[i].apropiacion_disponible;
      this.total.comprometido += (data[i].comprometido == null) ? 0: data[i].comprometido;
      this.total.obligado += (data[i].obligado == null) ? 0 :  data[i].obligado;
    }
    this.sinComprometer.inversion = (inversionApropiacion * (inversion_percent / 100));
    this.sinComprometer.funcionamiento = (funcionamientoApropiacion * (funcionamiento_percent / 100));
    this.noSIIFLoader =  (this.total.obligado == 0 ) ? true : false;
  }

  setError(error) {
    this.error = true;
    this.noData = false;
    this.loading = false;
    this.noSIIFLoader = false;
  }

  obtenerDatos(data) {
    this.error = false;
    this.noData = false;
    this.loading = true;
    this.noSIIFLoader = false;

    this.anio = data.anio;
    this.mes = data.mes;
    if (this.mes != null && this.anio != null) {
      this.sectorHttpService.getLiquidacionSectorInvFun(this.anio, this.mes)
        .subscribe(data => this.procesarData(data),
        error => this.setError(error));
    }
  }

  ngOnInit() {
    (this.sectorDataService._data !== null) ? this.obtenerDatos(this.sectorDataService._data) : null;
        this.sectorDataService.data.subscribe(data => {
      this.obtenerDatos(data);
    });
  }

}
