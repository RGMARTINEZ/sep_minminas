/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TopNavbarService } from './top-navbar.service';

describe('Service: TopNavbar', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TopNavbarService]
    });
  });

  it('should ...', inject([TopNavbarService], (service: TopNavbarService) => {
    expect(service).toBeTruthy();
  }));
});
