import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatNumber'
})
export class FormatNumberPipe implements PipeTransform {

  transform(value: any, args?: any): any {
/*
    var result1 = value.replace(',', '$');
    var result2 = result1.replace('.', '&');
    var result3 = result2.replace('$', '.');
    var result4 = result3.replace('&', ',');
*/
    const result = (value == null) ? '' : value.replace(/,/g, '#').replace('.', '@').replace(/#/g, '.').replace('@', ',');

    return result;
  }

}
