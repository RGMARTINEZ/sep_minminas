import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { END_POINT } from '../../globals';
import {Observable} from 'rxjs/Rx';
import { StorageManagerService } from './storage-manager.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

        // 27-04-2017
        // Andres Lopera
        // Define clase base para implementar los servicios
        // permitiendo interceptar todas las peticiones para el manejo de la autorización.

@Injectable()
export class HttpService {

  storage: StorageManagerService = new StorageManagerService();
  user: string = null;
  password: string = null;


  constructor(public http: Http) {
   }

  createAuthorizationHeader(headers: Headers) {

  this.user = atob(this.storage.retrieve("ASDFKSLFIN442387KJ4I7487"));
    this.password = atob(this.storage.retrieve("DDDLDDLIEJU3934875723MDM"));

    headers.append('Authorization', 'Basic ' +
      btoa( this.user + ':' + this.password ));

  }

    get(operation: string) {
      let headers = new Headers();
      this.createAuthorizationHeader(headers);
    return this.http.get(END_POINT + operation, { headers: headers }).map(this.extractData).catch(this.handleError);
  }


    post(operation: string, params: any) {
      let headers = new Headers();
      this.createAuthorizationHeader(headers);
    return this.http.post(END_POINT + operation, params, { headers: headers })
    .map(this.extractData)
    .catch(this.handleError);
  }


    private extractData(res: Response) {
      let body = res.json();
      return body || { };
    }


    private handleError (error: Response | any) {
      let errMsg: string;
      if (error instanceof Response) {
        // 27-04-2017
        // Andres Lopera
        // Se valida cuando no está autorizado el ususario y es direccionado al login
        // borrando los datos de acceso del local storage.

        /*
        if (error.status == 401) {
          let router: Router;
          let storage : StorageManagerService = new StorageManagerService();

            storage.remove("ASDFKSLFIN442387KJ4I7487");
            storage.remove("DDDLDDLIEJU3934875723MDM");
            router.navigate(['/login']);

        };
        */
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      return Observable.throw(errMsg);
    }
}
