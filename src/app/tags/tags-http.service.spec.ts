/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TagsHttpService } from './tags-http.service';

describe('Service: TagsHttp', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TagsHttpService]
    });
  });

  it('should ...', inject([TagsHttpService], (service: TagsHttpService) => {
    expect(service).toBeTruthy();
  }));
});
