import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectosDetalleComponent } from './proyectos-detalle.component';

describe('ProyectosDetalleComponent', () => {
  let component: ProyectosDetalleComponent;
  let fixture: ComponentFixture<ProyectosDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectosDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectosDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
