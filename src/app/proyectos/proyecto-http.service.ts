import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { Proyecto } from '../core/models/Proyecto';
import { Tag } from '../core/models/Tag';
import { HttpService } from '../core/services/http.service';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';



@Injectable()
export class ProyectoHttpService extends HttpService {

   constructor (http: Http) {
     super(http);
   }

  getListaProyectos(): Observable<any> {
    return this.get("Proyecto");
  }

  getByProyectoId(uej : string, rubro : string, anio : string) : Observable<any> {
    var param = {
      uej: uej,
      rubro : rubro,
      anio : anio
    }
     return this.post("Proyecto/getByProyectoId" , param);
  }

  getLiquidacionSectorPresidencia(anio : number) {
     return this.post("Proyecto/getLiquidacionSectorPresidencia?anio=" + anio, null );
  }


  getRankingEntidades(anio: number, mes : number, tipoPresupuesto: string,  tags : Array<Tag>): Observable<any> {

    var _tags = { "tags" : [] };
        for (var i = 0; i < tags.length; i++) {
          _tags.tags.push(tags[i].nombreTag);
        }
    (tags.length == 0) ? _tags = null : null;

    return this.post("Proyecto/getLiquidacionRanking?anio=" + anio + "&mes=" + mes + "&tipoPresupuesto=" + tipoPresupuesto, _tags );
  }

  getLiquidacionAcumulada(anio: number, mes : number, tipoPresupuesto : string, tags : Array<Tag>) {
    var _tags = { "tags" : [] };
    for (var i = 0; i < tags.length; i++) {
      _tags.tags.push(tags[i].nombreTag);
    }
   (tags.length == 0) ? _tags = null : null;
    return this.post("Proyecto/getLiquidacionAcumulada?anio=" + anio + "&mes=" + mes + "&tipoPresupuesto="  + tipoPresupuesto, _tags);
  }

  getLiquidacionByProyecto(anio: number, mes : number, tipoPresupuesto : string, tags : Array<Tag>) {
    var _tags = { "tags" : [] };
    for (var i = 0; i < tags.length; i++) {
      _tags.tags.push(tags[i].nombreTag);
    }
    (tags.length == 0) ? _tags = null : null;
    return this.post("Proyecto/getLiquidacionByProyecto?anio=" + anio + "&mes=" + mes + "&tipoPresupuesto="  + tipoPresupuesto, _tags);
  }

  getLiquidacionAcumuladaWithPorcentajes(anio: number, mes : number, tipoPresupuesto : string,  tags : Array<Tag>) {
    var _tags = { "tags" : [] };
    for (var i = 0; i < tags.length; i++) {
      _tags.tags.push(tags[i].nombreTag);
    }
    (tags.length == 0) ? _tags = null : null;
    return this.post('Proyecto/getLiquidacionAcumuladaWithPorcentajes?anio=' + anio + '&mes=' + mes + '&tipoPresupuesto='  + tipoPresupuesto, _tags);
  }

  updateProyecto(proyecto: Proyecto): Observable<any> {
    return this.post("Proyecto", proyecto);
  }

}
