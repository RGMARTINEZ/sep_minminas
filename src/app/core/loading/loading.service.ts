import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
@Injectable()
export class LoadingService {

  public isLoading : Subject<any> = new Subject();


  public setLoading(flat) {
    this.isLoading.next(flat);
  }

  constructor() { 
  }

}