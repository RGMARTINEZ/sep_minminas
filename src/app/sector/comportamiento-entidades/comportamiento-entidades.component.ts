import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ProyectoHttpService } from '../../proyectos/proyecto-http.service';
import { SectorDataService } from '../sector-data.service';

@Component({
  selector: 'app-comportamiento-entidades',
  templateUrl: './comportamiento-entidades.component.html',
  styleUrls: ['./comportamiento-entidades.component.css'],
  providers : [ProyectoHttpService]
})
export class ComportamientoEntidadesComponent implements OnInit {
  @Output() onGoToProjects = new EventEmitter();
  @Output() onGoToSeguimiento = new EventEmitter();

  anio : number = 2017;
  mes : number = 2;
  tipoPresupuesto : string = "TODOS";
  showAll : boolean = false;


  loading : boolean = false;
  noData : boolean = true;
  error : boolean = false;
  noSIIFLoader : boolean = false;
  data : Array<any> = new Array<any>();


  constructor(private proyectoHttpService: ProyectoHttpService, 
  private sectorDataService: SectorDataService) { }


  procesarDatos(entidades) {
    this.loading = false;
    this.noData = (entidades.lenght == 0) ? true : false;
    this.data = [];
    var todosNull = 0;
    var todos = 0;
    for (var i = 1; i < entidades.length; i++) {
      if (entidades[i].uej != "") {
      entidades[i].percent = entidades[i].obligado / entidades[i].apropiacion_disponible * 100;
      entidades[i].percent_meta = entidades[i].obligacion / entidades[i].apropiacion_disponible * 100;
      
//      entidades[i].percent = entidades[i].obligado / entidades[i].obligacion * 100;
      (entidades[i].obligado == null) ? todosNull += 1 : null;
      todos += 1;
      if (entidades[i].percent < entidades[i].percent_meta) {
 entidades[i]["class"] = "text-danger";
      } else {
entidades[i]["class"] = "text-success";
      }
      /*
        if (entidades[i].percent < 85) {
          entidades[i]["class"] = "text-danger";
        } else if (entidades[i].percent <= 90 ) {
          entidades[i]["class"] = "text-yellow";
        } else if (entidades[i].percent > 90) {
          entidades[i]["class"] = "text-success";
        }
        */
        this.data.push(entidades[i]);
      }
    }
     this.noSIIFLoader = (todosNull == todos) ? true : false;
  }


  obtenerDatos(data) {
        this.anio = data.anio;
        this.mes = data.mes;

        this.loading = true;
        this.noData = false;
        this.error = false;
        this.noSIIFLoader = false;
        this.data = [];      
        this.proyectoHttpService.getLiquidacionAcumuladaWithPorcentajes(this.anio, this.mes, this.tipoPresupuesto, [])
          .subscribe(entidades => this.procesarDatos(entidades),
          error => this.setError(error));
    }

  setError(error) {
    this.error = true;
    this.noData = false;
    this.loading = false;
    this.noSIIFLoader = false;
  }

  
    goToProjects(uej) {
      this.onGoToProjects.emit(uej);
    }

    goToSeguimiento(uej) {
      this.onGoToSeguimiento.emit(uej);
    }

  ngOnInit() {
    (this.sectorDataService._data !== null) ? this.obtenerDatos(this.sectorDataService._data) : null;
    this.sectorDataService.data.subscribe(data => {
      this.obtenerDatos(data);
    });
  }

}
