import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraficoGeneralComponent } from './grafico-general.component';

describe('GraficoGeneralComponent', () => {
  let component: GraficoGeneralComponent;
  let fixture: ComponentFixture<GraficoGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraficoGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraficoGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
