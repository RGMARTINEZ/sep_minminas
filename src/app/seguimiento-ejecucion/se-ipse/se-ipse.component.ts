import { Component, OnInit, Input } from '@angular/core';
import { SeguimientoEjecucionDataService } from '../seguimiento-ejecucion-data.service';

@Component({
  selector: 'app-se-ipse',
  templateUrl: './se-ipse.component.html',
  styleUrls: ['./se-ipse.component.less']
})
export class SeIpseComponent implements OnInit {
  @Input()
  set entidadHidden(entidades : any) {
    (entidades.length == 0) ? this.visible = true: null;
    for (var i = 0; i < entidades.length; i++) {
      (entidades[i] == "21-10-00") ? this.visible = false: null; 
    }
  }

  visible : boolean;

  ngOnInit() {
  }

  isVisible(entidad) {
    this.visible = (entidad == "21-10-00" || entidad == "TODOS") ? true : false;
  }

    constructor(private sData : SeguimientoEjecucionDataService) {
    this.sData.showEntidad.subscribe(entidad => {
      this.isVisible(entidad);
    });
  }

}
