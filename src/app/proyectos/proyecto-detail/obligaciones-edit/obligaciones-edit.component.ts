import { Component, ViewChild, OnInit, Output, EventEmitter } from '@angular/core';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { ActividadResumen } from '../../../core/models/ActividadResumen';

@Component({
  selector: 'app-obligaciones-edit',
  templateUrl: './obligaciones-edit.component.html',
  styleUrls: ['./obligaciones-edit.component.less']
})
export class ObligacionesEditComponent implements OnInit {

  @ViewChild('modal') modal: ModalComponent;
  @Output() closed = new EventEmitter(); 

  private tmp_actividad : Array<any> = new Array<any>();
  currentActividad : ActividadResumen = new ActividadResumen();

  public setActividad(actividad : ActividadResumen) {
    this.currentActividad = actividad;
    this.modal.open();
  }


  public close() {
    this.closed.emit({ andres: 1});
  }

  constructor() { }

  ngOnInit() {
  }

}
