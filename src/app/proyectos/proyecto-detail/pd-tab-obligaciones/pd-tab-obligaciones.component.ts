import { Component, ViewChild, OnInit } from '@angular/core';
import { Obligacion } from '../../../core/models/Obligacion';
import { Actividad } from '../../../core/models/Actividad';
import { ObligacionesEditComponent } from '../obligaciones-edit/obligaciones-edit.component';
import { ActividadResumen } from '../../../core/models/ActividadResumen';

@Component({
  selector: 'app-pd-tab-obligaciones',
  templateUrl: './pd-tab-obligaciones.component.html',
  styleUrls: ['./pd-tab-obligaciones.component.less']
})
export class PdTabObligacionesComponent implements OnInit {

  @ViewChild(ObligacionesEditComponent) obligacionesEdit: ObligacionesEditComponent;
  public actividades: Array<Actividad>;
  public actividades_obligaciones: Array<any> = new Array<any>();
  public total_mes: Array<number> = new Array<number>();
  public total_obligaciones: number = 0;
  private actividad: ActividadResumen = new ActividadResumen();

  public changeObligaciones(event) {
    this.updateObligaciones();
  }

  public editActividad(actividad: ActividadResumen) {
    this.obligacionesEdit.setActividad(actividad);
  }


  private obtenerActividad(idActividad : number) {
    for (var i = 0; i < this.actividades.length; i++) {
      if (this.actividades[i].idActividad == idActividad) {
        return this.actividades[i];
      }
    }
  }

  private obtenerCompromiso(idCompromiso: number, actividad : Actividad) {
    var result = null;
    for (var i = 0 ; actividad.compromiso.length; i++) {
      if (idCompromiso == actividad.compromiso[i].idCompromiso) {
        result = actividad.compromiso[i];
        break;
      }
    }
    return result;
  }

  public getObligaciones() {
    var result : Array<Actividad> = new Array<Actividad>();
    for (var i = 0; i < this.actividades_obligaciones.length; i++) {
      var tmp_actividad : Actividad = this.obtenerActividad( this.actividades_obligaciones[i].idActividad);
      var tmp_obligaciones : Array<Obligacion> = new Array<Obligacion>();

      for (var j=0; j < this.actividades_obligaciones[i].obligaciones.length; j++) {
        var tmp_obligacion : Obligacion = new Obligacion();
        if (this.actividades_obligaciones[i].obligaciones[j].id != null) {
            tmp_obligacion = this.obtenerCompromiso(this.actividades_obligaciones[i].obligaciones[j].id, tmp_actividad);
        } else {
          tmp_obligacion.mes = j + 1;
        }
        tmp_obligacion.valor = this.actividades_obligaciones[i].obligaciones[j].valor;
        (tmp_obligacion.valor > 0) ? tmp_obligaciones.push(tmp_obligacion) : null;
      }

      tmp_actividad.obligacion = tmp_obligaciones;
      result.push(tmp_actividad);
    }
    return result;
  }

  constructor() {
    this.actividad = new ActividadResumen();
    for (var i = 0; i < 12; i++) {
      this.total_mes.push(0);
    }
  }


  public CargarActividades(actividades: Array<Actividad>) {
    let obligacion: any;
    for (var i = 0; i < actividades.length; i++) {
      this.actividad = new ActividadResumen();
      this.actividad.idActividad = actividades[i].idActividad;
      this.actividad.nombreActividad = actividades[i].nombreActividad;
      for (var j = 0; j < actividades[i].obligacion.length; j++) {
        obligacion = actividades[i].obligacion[j];
        this.actividad.obligaciones[obligacion.mes - 1].id = obligacion.idobligacion;
        this.actividad.obligaciones[obligacion.mes - 1].valor = obligacion.valor;
        this.actividad.total += obligacion.valor;
        this.total_mes[obligacion.mes - 1] = this.total_mes[obligacion.mes - 1] + obligacion.valor;
        this.total_obligaciones += obligacion.valor;
      }
      this.actividades_obligaciones.push(this.actividad);
    }
  }

  public updateObligaciones() {
    this.total_obligaciones = 0;
    this.total_mes = [];
    for (var i = 0; i < 12; i++) {
      this.total_mes.push(0);
    }
    for (var i = 0; i < this.actividades_obligaciones.length; i++) {
      this.actividades_obligaciones[i].total = Number(0);
      for (var j = 0; j < this.actividades_obligaciones[i].obligaciones.length; j++) {
        this.actividades_obligaciones[i].total += Number(this.actividades_obligaciones[i].obligaciones[j].valor);
        this.total_mes[j] += Number(this.actividades_obligaciones[i].obligaciones[j].valor);
        this.total_obligaciones += Number(this.actividades_obligaciones[i].obligaciones[j].valor);
      }
    }
  }



  ngOnInit() {
    this.CargarActividades(this.actividades);
  }

}
