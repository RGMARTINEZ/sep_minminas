import { TestBed, inject } from '@angular/core/testing';

import { SectorDataService } from './sector-data.service';

describe('SectorDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SectorDataService]
    });
  });

  it('should ...', inject([SectorDataService], (service: SectorDataService) => {
    expect(service).toBeTruthy();
  }));
});
