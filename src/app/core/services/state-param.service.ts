import { Injectable } from '@angular/core';
import { StorageManagerService }  from './storage-manager.service';

@Injectable()
export class StateParamService {

  stateParams = [];
  private storageManagerService : StorageManagerService = new StorageManagerService();
  

  constructor() { }

  setParams(name : string, object : any) {

    var found = false;
    for (var i = 0; i < this.stateParams.length; i++) {
      if (this.stateParams[i].name == name) {
        this.stateParams[i].payload = object;
        found = true;
        break;
      }
    }
    (!found) ? this.stateParams.push( { name : name , payload : object } ): null; 
    this.storageManagerService.store(name, object);

  }

  getParams(name : string) {

    var result = null;
    for (var i = 0; i < this.stateParams.length; i++) {
      if (this.stateParams[i].name == name) {
        result = this.stateParams[i].payload;
        break;
      }
    }
    
    if (result == null) {
        result = JSON.parse( this.storageManagerService.retrieve(name) );
        (result != null) ? this.stateParams.push({ 
          name : name,
          payload : result 
        }) : null;
    }

    return result;
  }

}
