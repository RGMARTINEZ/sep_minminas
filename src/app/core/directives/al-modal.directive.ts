import { Directive, ElementRef,  Renderer, AfterViewInit } from '@angular/core';
declare var jQuery: any;

@Directive({
  selector: '[appAlModal]'
})
export class AlModalDirective {

  constructor(private el: ElementRef, private renderer: Renderer) { }

    ngAfterViewInit() {
      jQuery(this.el.nativeElement).modal()                      // initialized with defaults
      //jQuery(this.el.nativeElement).modal({ keyboard: false })   // initialized with no keyboard
      //jQuery(this.el.nativeElement).modal('show')
    }

}
