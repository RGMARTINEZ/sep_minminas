import { TestBed, inject } from '@angular/core/testing';
import { LiquidacionesHttpService } from './liquidaciones-http.service';

describe('LoginHttpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LiquidacionesHttpService]
    });
  });

  it('should ...', inject([LiquidacionesHttpService], (service: LiquidacionesHttpService) => {
    expect(service).toBeTruthy();
  }));
});
