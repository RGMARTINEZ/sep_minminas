/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ArrayFilterService } from './array-filter.service';

describe('Service: ArrayFilter', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ArrayFilterService]
    });
  });

  it('should ...', inject([ArrayFilterService], (service: ArrayFilterService) => {
    expect(service).toBeTruthy();
  }));
});
