import { DocumentosHttpService } from '../documentos-http.service';
import { Component, OnInit } from '@angular/core';
import { PaginationList } from '../../core/models/paginationsList';
import { Proyecto } from '../../core/models/Proyecto';
import { ProyectoDataService } from '../../proyectos/proyecto-data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ParamFilter } from '../../core/models/paramFilter';
import { Tag } from '../../core/models/Tag';
import { STATE_PARAM_GLOBAL, END_POINT } from '../../globals';
import { TagsHttpService } from '../../tags/tags-http.service';
import { StateParamService } from '../../core/services/state-param.service';
import { GlobalVariablesService } from '../../core/services/global-variables.service';
import { StorageManagerService } from '../../core/services/storage-manager.service';

function getWindows(): any {
  return window;
}


@Component({
  selector: 'app-documentos-list',
  templateUrl: './documentos-list.component.html',
  styleUrls: ['./documentos-list.component.css'],
  providers: [TagsHttpService, DocumentosHttpService]
})
export class DocumentosListComponent extends PaginationList implements OnInit {

  currentOptionsConfig = {
    labelField: 'nombreTag',
    valueField: 'nombreTag',
    searchField: ['nombreTag', 'idTag'],
    maxItems: 10,
    highlight: false,
    create: false,
    persist: true,
    plugins: ['dropdown_direction', 'remove_button'],
    dropdownDirection: 'down'
  };

  mes: number= 1;
  anio: number = 2018;


  today: number = Date.now();
 




  showData : boolean = false;
  firma: string = '';
  proyeccion: string = '';
  sub : any;
  result : any;
  public URL: String = END_POINT;

  private subRouter: any;
  token: string;
  authorization = '';

  filterByFirma: ParamFilter = new ParamFilter();
  filterByProyeccion: ParamFilter = new ParamFilter();

  newDocument() {
    this.router.navigate(['app/documentos/nuevo']);
  }

  onDownloadFile(adjuntoId: string) {
    alert('download file');
  }

    tag = {
      items: [],
      dataAutocomplete: [],
      data: [],
      errorMessage: "",
      setItems: function (tags) {
        for (var i = 0; i < tags.length; i++) {
          this.items.push(tags[i].nombreTag);
        }
      },
      setTags: function (tags) {
        this.data = tags;
        for (var i = 0; i < tags.length; i++) {
          this.dataAutocomplete.push(tags[i]);
        }
      },
      getTags: function () {
        var result = [];
        for (var i = 0; i < this.items.length; i++) {
          var tmpTag = new Tag(null, this.items[i]);

          for (var j = 0; j < this.data.length; j++) {
            if (this.data[j].nombreTag == this.items[i]) {
              tmpTag.idTag = this.data[j].idTag;
              tmpTag.nombreTag = this.data[j].nombreTag;
            }
          }
          result.push(tmpTag);
        }
        return result;
      }
    }

    getTags() {
      this.tagsHttpService.getTags()
        .subscribe(tags => this.tag.setTags(tags),
        error => this.tag.errorMessage = <any>error);
    }


  procesarData(documentos) {
    this.setData(documentos);
    this.filterData();
  }


  setError(error) {
    this.error = true;
    this.noData = false;
    this.loading = false;
  }


  obtenerDatos() {
    this.loading = true;
    this.noData = false;
    this.error = false;
    this.data = [];
    this.documentosHttpService.getListaDocumentosByFilter(this.anio, this.mes)
    .subscribe(documentos => this.procesarData(documentos),
    error => this.setError(error));
  }

  public filterData() {
    this.paramsFilters = new Array<ParamFilter>();
    if (this.firma !== '') {
      this.filterByFirma.precision = true;
      this.filterByFirma.key = 'firma';
      this.filterByFirma.value = this.firma;
      this.paramsFilters.push(this.filterByFirma);
    }
    if (this.proyeccion !== '') {
      this.filterByProyeccion.precision = true;
      this.filterByProyeccion.key = 'proyeccion';
      this.filterByProyeccion.value = this.proyeccion;
      this.paramsFilters.push(this.filterByProyeccion);
    }
    super.filter();
  }


  public downloadfile(adjunto: string) {
    //getWindows().open(this.URL + 'Normatividad/files/' + adjunto, '');




let anchor = document.createElement("a");
let file = this.URL + 'Normatividad/files/' + adjunto;

let headers = new Headers();
headers.append('Authorization', this.authorization,);

fetch(file, { headers })
    .then(response => response.blob())
    .then(blobby => {
        let objectUrl = window.URL.createObjectURL(blobby);

        anchor.href = objectUrl;
        anchor.download = 'Archivo';
        anchor.click();

        window.URL.revokeObjectURL(objectUrl);
    });
  }

  constructor(private documentosHttpService: DocumentosHttpService,
     private tagsHttpService: TagsHttpService,
     private proyectoDataService: ProyectoDataService,
     private route: ActivatedRoute,
    private router: Router,
    private storageManagerService: StorageManagerService,
    private stateParamService: StateParamService,
    private globalVariablesService : GlobalVariablesService) {
        super();
    }


  ngOnInit() {

  
let d = new Date();
var n = d.getMonth();
this.mes = n + 1;

    this.getTags();

    this.obtenerDatos();

    let user = atob(this.storageManagerService.retrieve('ASDFKSLFIN442387KJ4I7487'));
    let password =  atob(this.storageManagerService.retrieve('DDDLDDLIEJU3934875723MDM'));
    this.authorization = 'Basic ' + btoa( user + ':' + password );
  }

  changedItemsTag() {
    super.filter();
  }

  onChangedData() {
    this.noData = (this.data.length === 0) ? true : false;
  }

  onAfterFilterSelectedItems() {
    let tmpSelectedItems = [];
    if (this.tag.items.length > 0) {
      for (let j = 0; j < this.selectedItems.length; j++) {
        let found = false;
        for (let k = 0; k <  this.selectedItems[j].tag.length; k++) {
          for (let i = 0; i < this.tag.items.length; i++) {
            if (this.tag.items[i] === this.selectedItems[j].tag[k].nombreTag) {
              tmpSelectedItems.push(this.selectedItems[j]);
              found = true;
              break;
            }
          }
          if (found) { break; }
        }
      }
      this.selectedItems = tmpSelectedItems;
    }
  }

}
