import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { ProyectoDataService } from '../proyecto-data.service';
// MODELS
import { Proyecto } from '../../core/models/Proyecto';
import { Tag } from '../../core/models/Tag';
import { Actividad } from '../../core/models/Actividad';
import { Compromiso } from '../../core/models/Compromiso';
import { Obligacion } from '../../core/models/Obligacion';

// COMPONENTS
import { PdTabDetailComponent } from './pd-tab-detail/pd-tab-detail.component';
import { PdTabCompromisosComponent } from './pd-tab-compromisos/pd-tab-compromisos.component';
import { PdTabObligacionesComponent } from './pd-tab-obligaciones/pd-tab-obligaciones.component';
// HTTPS SERVICES
import { TagsHttpService } from '../../tags/tags-http.service';
import { ProyectoHttpService } from '../proyecto-http.service';
import { LoadingService } from '../../core/loading/loading.service';

import { MessageService } from '../../core/services/message.service';

@Component({
  selector: 'app-proyecto-detail',
  templateUrl: './proyecto-detail.component.html',
  styleUrls: ['./proyecto-detail.component.less'],
  providers: [TagsHttpService, ProyectoHttpService]
})
export class ProyectoDetailComponent implements OnInit, OnDestroy {

  @ViewChild(PdTabDetailComponent) detailComponent: PdTabDetailComponent;
  @ViewChild(PdTabCompromisosComponent) compromisosComponent: PdTabCompromisosComponent;
  @ViewChild(PdTabObligacionesComponent) obligacionesComponent: PdTabObligacionesComponent;

  currentTab: String = "detalle";
  proyecto: Proyecto = new Proyecto();
  private sub: any;

  tag = {
    items: [],
    dataAutocomplete: [],
    data: [],
    errorMessage: "",
    setItems: function (tags) {
      for (var i = 0; i < tags.length; i++) {
        this.items.push(tags[i].nombreTag);
      }
    },
    setTags: function (tags) {
      this.data = tags;
      for (var i = 0; i < tags.length; i++) {
        this.dataAutocomplete.push(tags[i].nombreTag);
      }
    },
    getTags: function () {
      var result = [];
      for (var i = 0; i < this.items.length; i++) {
        var tmpTag = new Tag(null, this.items[i]);

        for (var j = 0; j < this.data.length; j++) {
          if (this.data[j].nombreTag == this.items[i]) {
            tmpTag.idTag = this.data[j].idTag;
            tmpTag.nombreTag = this.data[j].nombreTag;
          }
        }
        result.push(tmpTag);
      }
      return result;
    }
  }

  private obtenerCompromisosByIdActividad(idActividad: number, actividades: Array<Actividad>) {
    let result: Array<Compromiso> = new Array<Compromiso>();
    for (var i = 0; i < actividades.length; i++) {
      if (actividades[i].idActividad == idActividad) {
        result = actividades[i].compromiso;
        break;
      }
    }
    return result;
  }
  private obtenerObligacionesByIdActividad(idActividad: number, actividades: Array<Actividad>) {
    let result: Array<Obligacion> = new Array<Obligacion>();
    for (var i = 0; i < actividades.length; i++) {
      if (actividades[i].idActividad == idActividad) {
        result = actividades[i].obligacion;
        break;
      }
    }
    return result;
  }

  public actualizarProyecto() {
    let tmp_actividades_compromisos: Array<Actividad> = new Array<Actividad>();
    let tmp_actividades_obligaciones: Array<Actividad> = new Array<Actividad>();

    tmp_actividades_compromisos = this.compromisosComponent.getCompromisos();
    tmp_actividades_obligaciones = this.obligacionesComponent.getObligaciones();

    this.proyecto.tag = new Array<Tag>();
    var tags = this.tag.getTags();
    for (var i = 0; i < tags.length; i++) {
      this.proyecto.tag.push(tags[i]);
    }

    for (var i = 0; i < this.proyecto.actividad.length; i++) {
      this.proyecto.actividad[i].compromiso = this.obtenerCompromisosByIdActividad(
        this.proyecto.actividad[i].idActividad,
        tmp_actividades_compromisos);

      this.proyecto.actividad[i].obligacion = this.obtenerObligacionesByIdActividad(
        this.proyecto.actividad[i].idActividad,
        tmp_actividades_obligaciones);
    }
    this.loadingService.setLoading(true);
    (this.proyecto.alias == "") ? this.proyecto.alias = null : null;
    this.proyectoHttpService.updateProyecto(this.proyecto).subscribe(response => this.updateSuccess(response), faild => this.updateError(faild));

  }


  updateSuccess(message) {
    this.loadingService.setLoading(false);
    this.messageService.showSuccess(null, "Proyecto Actualizado exitosamente.");
  }

  updateError(faild) {
    this.loadingService.setLoading(false);
     this.messageService.showError(null, "Ocurrió un error al intentar actualizar el proyecto");
  }

  getTags() {
    this.tagsHttpService.getTags()
      .subscribe(tags => this.tag.setTags(tags),
      error => this.tag.errorMessage = <any>error);
  }


  constructor(private proyectoDataService: ProyectoDataService,
    private tagsHttpService: TagsHttpService,
    private proyectoHttpService: ProyectoHttpService,
    private messageService: MessageService,
    private loadingService: LoadingService) {

/*
      this.proyectoDataService.proyecto.subscribe(proyecto => {
        this.proyecto = proyecto;
      })
      */
  }

  ngOnInit() {
    this.getTags();
    this.proyecto = this.proyectoDataService.getData();
    this.detailComponent.proyecto = this.proyecto;
    this.compromisosComponent.actividades = this.proyecto.actividad;
    this.obligacionesComponent.actividades = this.proyecto.actividad;
    this.tag.setItems(this.proyecto.tag);
  }

  ngOnDestroy() {
  }

}