import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { HttpService } from '../core/services/http.service';

@Injectable()
export class SectorHttpService extends HttpService {

   constructor (http: Http) {
     super(http);
   }


  getLiquidacionSectorInvFun(anio: number, mes : number) {
    return this.post("Proyecto/getLiquidacionSectorInvFun?anio=" + anio + "&mes=" + mes, null);
  }

}
