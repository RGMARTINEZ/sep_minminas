import { Tag } from './Tag';
import { Actividad } from './Actividad';
import { ProyectoRecurso } from './ProyectoRecurso';

export class Proyecto {
    
    anio: number;
    bpin: string;
    alias : string;
    proyectoRecurso: Array<ProyectoRecurso>;
    actividad: Array<Actividad>;
    tag: Array<Tag>;
    uej: string;
    rubro: string;
    nombreuej: string;
    tipo: string;
    fuente: string;
    rec: string;
    sit: string;
    descripcion: string;

}

