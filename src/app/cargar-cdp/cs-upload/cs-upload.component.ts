import { Component, OnInit } from '@angular/core';
import { FileSelectDirective, FileUploader } from '../../../../node_modules/ng2-file-upload';
import {FormControl, FormGroup} from '@angular/forms';
import { END_POINT } from '../../globals';
import { LoadingService } from '../../core/loading/loading.service';
import { MessageService } from '../../core/services/message.service';
import { StorageManagerService } from '../../core/services/storage-manager.service';

@Component({
  selector: 'app-cs-upload',
  templateUrl: './cs-upload.component.html',
  styleUrls: ['./cs-upload.component.less']
})

export class CsUploadComponent implements OnInit {


  semanas: any = [
    { id: 1, name: 'Uno' },
    { id: 2, name: 'Dos' },
    { id: 3, name: 'Tres' },
    { id: 4, name: 'Cuatro' },
    { id: 5, name: 'Cinco' }];


  meses: any = [
    { id: 1, name: 'Enero' },
    { id: 2, name: 'Febrero' },
    { id: 3, name: 'Marzo' },
    { id: 4, name: 'Abril' },
    { id: 5, name: 'Mayo' },
    { id: 6, name: 'Junio' },
    { id: 7, name: 'Julio' },
    { id: 8, name: 'Agosto' },
    { id: 9, name: 'Septiembre' },
    { id: 10, name: 'Octubre' },
    { id: 11, name: 'Noviembre' },
    { id: 12, name: 'Diciembre' }];


  //anio = 2017;
  anio : number = new Date ().getFullYear();
  mes = 1;
  semana = 1;
  authorization = '';

  public URL: String = END_POINT;


  table_response: any = {
    header: [],
    body: []
  };


  public uploader: FileUploader = new FileUploader({});

  public procesorResponse(siif) {
    let obj = siif[0];
    this.table_response.header = Object.keys(obj);
    for (let i = 0; i < siif.length; i++) {
      this.table_response.body.push(Object.keys(siif[i]).map(key => obj[key]));
    }
  }

  public uploadFile() {
      this.loadingService.setLoading(true);
      let file = this.uploader.queue[0];
      file.withCredentials = false;

      let URL = this.URL + 'SIIF/upload' + '?anio=' + this.anio + '&mes=' + this.mes + '&semana=' + this.semana;
      file.url = URL;
      file.method = 'POST';
      this.uploader.authToken = this.authorization;
      this.uploader.uploadItem(file);
  }


  constructor(private loadingService: LoadingService,
  private messageService: MessageService,
  private storageManagerService: StorageManagerService) {

  }

  ngOnInit() {
       let user = atob(this.storageManagerService.retrieve('ASDFKSLFIN442387KJ4I7487'));
   let password =  atob(this.storageManagerService.retrieve('DDDLDDLIEJU3934875723MDM'));
   this.authorization = 'Basic ' + btoa( user + ':' + password );

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.messageService.showInfo(null, 'Archivo cargado Exitosamente');
      this.loadingService.setLoading(false);
      const result = JSON.parse(response);
      this.procesorResponse(result);
    };

    this.uploader.onErrorItem = (item: any, response: any, status: any, headers: any) => {
      this.messageService.showError(null, 'Ocurrió un error al intentar cargar el archivo');
      const responsePath = JSON.parse(response);
      this.loadingService.setLoading(false);
    };

  }

}
