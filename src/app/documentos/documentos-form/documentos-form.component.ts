import { Component, OnInit } from '@angular/core';
import { TagsHttpService } from '../../tags/tags-http.service';
import { Tag } from '../../core/models/Tag';
import { DocumentosHttpService } from '../documentos-http.service';
import { Router } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';
import { LoadingService } from '../../core/loading/loading.service';
import { END_POINT } from '../../globals';
import { StorageManagerService } from '../../core/services/storage-manager.service';
import { UuidService } from '../../core/services/uuid.service';
import { MessageService } from '../../core/services/message.service';

@Component({
  selector: 'app-documentos-form',
  templateUrl: './documentos-form.component.html',
  styleUrls: ['./documentos-form.component.css'],
  providers: [TagsHttpService, DocumentosHttpService, UuidService]
})
export class DocumentosFormComponent implements OnInit {

  documento = {
    tipoNormatividad: '',
    fecha: '',
    consecutivo : '',
    proyeccion: '',
    epigrafe: '',
    firma: '',
    adjunto: '',
    tag: []
  };

  public uploader: FileUploader = new FileUploader({});
  public URL: String = END_POINT;
  authorization : string = "";
  fileid = 0;


  currentOptionsConfig = {
    labelField: 'nombreTag',
    valueField: 'nombreTag',
    searchField: ['nombreTag', 'idTag'],
    maxItems: 10,
    highlight: false,
    create: false,
    persist: true,
    plugins: ['dropdown_direction', 'remove_button'],
    dropdownDirection: 'down'
  };


  tag = {
    items: [],
    dataAutocomplete: [],
    data: [],
    errorMessage: '',
    setItems: function (tags) {
      for (let i = 0; i < tags.length; i++) {
        this.items.push(tags[i].nombreTag);
      }
    },
    setTags: function (tags) {
      this.data = tags;
      for (let i = 0; i < tags.length; i++) {
        this.dataAutocomplete.push(tags[i]);
      }
    },
    getTags: function (): Array<any> {
      let result = [];
      for (let i = 0; i < this.items.length; i++) {
        let tmpTag = new Tag(null, this.items[i]);

        for (let j = 0; j < this.data.length; j++) {
          if (this.data[j].nombreTag == this.items[i]) {
            tmpTag.idTag = this.data[j].idTag;
            tmpTag.nombreTag = this.data[j].nombreTag;
          }
        }
        result.push(tmpTag);
      }
      return result;
    }
  }

  public uploadFile(idNormatividad: any) {
    this.loadingService.setLoading(true);
    let file = this.uploader.queue[0];
    file.withCredentials = false;
    //this.fileid = this.uuid.newId();
    this.fileid = new Date().getTime();
    let URL = this.URL + 'Normatividad/upload?id=' + idNormatividad;
    file.url = URL;
    file.method = "POST";
    this.uploader.authToken = this.authorization;
    this.uploader.uploadItem(file);
    //this.loadingService.setLoading(false);
    this.router.navigate(['app/documentos']);
}

  onChangeTipoNormaticvidad() {
  }

  onChangeProyeccion() {
  }

  onChangeFirma() {
  }


  getTags() {
    this.tagsHttpService.getTags()
      .subscribe(tags => this.tag.setTags(tags),
      error => this.tag.errorMessage = <any>error);
  }

  save() {
    this.loadingService.setLoading(true);
    let tags = this.tag.getTags();
    for (let i = 0; i < tags.length; i++) {
      this.documento.tag.push({ nombreTag: tags[i].nombreTag});
    }
    this.documentosHttpService.saveNormatividad(this.documento).subscribe(
      response => this.onSaved(response), faild => alert(faild));
  }

  onSaved(response: any) {

    console.log('VALOR INDICADO' , this.uploader.queue[0])
    if(this.uploader.queue[0] != null || this.uploader.queue[0] != undefined){
      this.uploadFile(response.idNormatividad);
    } else {

      this.loadingService.setLoading(false);
      this.router.navigate(['app/documentos']);

    }
    //this.messageService.showInfo(null, "Documento creado exitosamente.");

   
  }

  onFaild() {
    this.loadingService.setLoading(false);
    this.messageService.showError(null, "Ocurrió un error al intentan crear el documento.");
  }

  onBackPress() {
    this.router.navigate(['app/documentos']);
  }

  constructor(private loadingService : LoadingService,
    private tagsHttpService: TagsHttpService,
    private documentosHttpService: DocumentosHttpService,
    private storageManagerService: StorageManagerService,
    public router: Router,
    public uuid: UuidService,
    public messageService: MessageService) { }

  ngOnInit() {

    let user = atob(this.storageManagerService.retrieve("ASDFKSLFIN442387KJ4I7487"));
    let password =  atob(this.storageManagerService.retrieve("DDDLDDLIEJU3934875723MDM"));
    this.authorization = 'Basic ' + btoa( user + ':' + password );

    this.getTags();

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.messageService.showInfo(null, "Archivo cargado Exitosamente");
      const result = JSON.parse(response);
      this.loadingService.setLoading(false);
      //this.save(result.adjunto);
      /*
      var result = JSON.parse(response);
      this.procesorResponse(result);
      */
    };

    this.uploader.onErrorItem = (item: any, response: any, status: any, headers: any) => {
      this.messageService.showError(null, "Ocurrió un error al intentar cargar el archivo");
     this.loadingService.setLoading(false);
      /*
      this.messageService.showError(null, "Ocurrió un error al intentar cargar el archivo");
      var responsePath = JSON.parse(response);
      */
    };
  }

}
