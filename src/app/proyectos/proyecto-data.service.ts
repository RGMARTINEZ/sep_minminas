import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Proyecto } from '../core/models/Proyecto';


@Injectable()
export class ProyectoDataService {

  public proyecto : Subject<any> = new Subject();
  public proyecto_data : Proyecto

  
  public setData(proyecto : Proyecto) {
    this.proyecto_data = proyecto;
    this.proyecto.next(proyecto);
  }

  public getData(): Proyecto {
    return this.proyecto_data
  }

  constructor() { 
  }

}