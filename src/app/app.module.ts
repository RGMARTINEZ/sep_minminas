import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';
//import { TagInputModule } from 'ng2-tag-input';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';
import {RlTagInputModule} from 'angular2-tag-input';
import { PaginationModule } from 'ng2-bootstrap';
import { PaginationConfig } from 'ng2-bootstrap/pagination/pagination.config';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { routing } from './app.routing';
import { LoginComponent } from './login/login.component';
import { TopNavbarComponent } from './core/top-navbar/top-navbar.component';
import { SidebarComponent } from './core/sidebar/sidebar.component';
import { EchartBarDirective } from './core/directives/echart-bar.directive';
import { AlSelectDirective } from './core/directives/al-select.directive';
import { SeguimientoEjecucionComponent } from './seguimiento-ejecucion/seguimiento-ejecucion.component';
import { SeMinminasComponent } from './seguimiento-ejecucion/se-minminas/se-minminas.component';
import { SeMinminasCompromisosComponent } from './seguimiento-ejecucion/se-minminas/se-minminas-compromisos/se-minminas-compromisos.component';
import { SeMinminasObligacionesComponent } from './seguimiento-ejecucion/se-minminas/se-minminas-obligaciones/se-minminas-obligaciones.component';
import { SeAnhComponent } from './seguimiento-ejecucion/se-anh/se-anh.component';
import { SeAnmComponent } from './seguimiento-ejecucion/se-anm/se-anm.component';
import { SeCregComponent } from './seguimiento-ejecucion/se-creg/se-creg.component';
import { SeIpseComponent } from './seguimiento-ejecucion/se-ipse/se-ipse.component';
import { SeSgcComponent } from './seguimiento-ejecucion/se-sgc/se-sgc.component';
import { SeUpmeComponent } from './seguimiento-ejecucion/se-upme/se-upme.component';
import { SeAnhCompromisosComponent } from './seguimiento-ejecucion/se-anh/se-anh-compromisos/se-anh-compromisos.component';
import { SeAnhObligacionesComponent } from './seguimiento-ejecucion/se-anh/se-anh-obligaciones/se-anh-obligaciones.component';
import { SeAnmObligacionesComponent } from './seguimiento-ejecucion/se-anm/se-anm-obligaciones/se-anm-obligaciones.component';
import { SeAnmCompromisosComponent } from './seguimiento-ejecucion/se-anm/se-anm-compromisos/se-anm-compromisos.component';
import { SeCregCompromisosComponent } from './seguimiento-ejecucion/se-creg/se-creg-compromisos/se-creg-compromisos.component';
import { SeCregObligacionesComponent } from './seguimiento-ejecucion/se-creg/se-creg-obligaciones/se-creg-obligaciones.component';
import { SeIpseCompromisosComponent } from './seguimiento-ejecucion/se-ipse/se-ipse-compromisos/se-ipse-compromisos.component';
import { SeIpseObligacionesComponent } from './seguimiento-ejecucion/se-ipse/se-ipse-obligaciones/se-ipse-obligaciones.component';
import { SeSgcCompromisosComponent } from './seguimiento-ejecucion/se-sgc/se-sgc-compromisos/se-sgc-compromisos.component';
import { SeSgcObligacionesComponent } from './seguimiento-ejecucion/se-sgc/se-sgc-obligaciones/se-sgc-obligaciones.component';
import { SeUpmeCompromisosComponent } from './seguimiento-ejecucion/se-upme/se-upme-compromisos/se-upme-compromisos.component';
import { SeUpmeObligacionesComponent } from './seguimiento-ejecucion/se-upme/se-upme-obligaciones/se-upme-obligaciones.component';
import { CargarSiifComponent } from './cargar-siif/cargar-siif.component';
import { CargarCdpComponent } from './cargar-cdp/cargar-cdp.component';

import { ProyectosComponent } from './proyectos/proyectos.component';
import { ProyectoListComponent } from './proyectos/proyecto-list/proyecto-list.component';
import { ProyectoDetailComponent } from './proyectos/proyecto-detail/proyecto-detail.component';
import { PdTabDetailComponent } from './proyectos/proyecto-detail/pd-tab-detail/pd-tab-detail.component';
import { PdTabCompromisosComponent } from './proyectos/proyecto-detail/pd-tab-compromisos/pd-tab-compromisos.component';
import { PdTabObligacionesComponent } from './proyectos/proyecto-detail/pd-tab-obligaciones/pd-tab-obligaciones.component';
import { PdTabEjecucionComponent } from './proyectos/proyecto-detail/pd-tab-ejecucion/pd-tab-ejecucion.component';
import { CsListComponent } from './cargar-siif/cs-list/cs-list.component';
import { CsUploadComponent } from './cargar-siif/cs-upload/cs-upload.component';
//import { CargarSiifHttpService } from './cargar-siif/cargar-siif-http.service';
import { EchartsDirective } from './core/directives/echarts.directive';
import { MultiselectDirective } from './core/directives/multiselect.directive';
import { AlModalDirective } from './core/directives/al-modal.directive';
import { CompromisosEditComponent } from './proyectos/proyecto-detail/compromisos-edit/compromisos-edit.component';
import { ObligacionesEditComponent } from './proyectos/proyecto-detail/obligaciones-edit/obligaciones-edit.component';
import { TagsComponent } from './tags/tags.component';
import { RankingEntidadesComponent } from './ranking-entidades/ranking-entidades.component';
import { SeguimientoProyectoComponent } from './seguimiento-proyecto/seguimiento-proyecto.component';
import { SeguimientoEntidadComponent } from './seguimiento-entidad/seguimiento-entidad.component';
import { RoundNumberPipe } from './core/pipe/round-number.pipe';
import { SectorComponent } from './sector/sector.component';
import { ResumenCifrasComponent } from './sector/resumen-cifras/resumen-cifras.component';
import { ResumenEjecucionComponent } from './sector/resumen-ejecucion/resumen-ejecucion.component';
import { VariablesRelevantesComponent } from './sector/variables-relevantes/variables-relevantes.component';
import { ComportamientoEntidadesComponent } from './sector/comportamiento-entidades/comportamiento-entidades.component';
import { ProyectoDataService } from './proyectos/proyecto-data.service';
import { OrderByPipe } from './core/pipe/order-by.pipe';
import { BtnMenuDirective } from './core/directives/btn-menu.directive';
import { LoadingComponent } from './core/loading/loading.component';
import { LoadingService } from './core/loading/loading.service';
import { MessageService } from './core/services/message.service';
import { StorageManagerService } from './core/services/storage-manager.service';
import { StateParamService } from './core/services/state-param.service';
import { FormatNumberPipe } from './core/pipe/format-number.pipe';


import {NgSelectizeModule} from 'ng-selectize';
import { PresupuestoComponent } from './presupuesto/presupuesto.component';
import { GraficoGeneralComponent } from './presupuesto/grafico-general/grafico-general.component';
import { ProyectosDetalleComponent } from './presupuesto/proyectos-detalle/proyectos-detalle.component';
import { ValidateUserComponent } from './validate-user/validate-user.component';
import { DocumentosListComponent } from './documentos/documentos-list/documentos-list.component';
import { DocumentosFormComponent } from './documentos/documentos-form/documentos-form.component';
import { DocumentosComponent } from './documentos/documentos.component';

import { CdpComponent } from './cdp/cdp.component';
import { LiquidacionesComponent } from './liquidaciones/liquidaciones.component';




@NgModule({
  declarations: [
    FileSelectDirective,
    AppComponent,
    MainComponent,
    LoginComponent,
    TopNavbarComponent,
    SidebarComponent,
    EchartBarDirective,
    AlSelectDirective,
    SeguimientoEjecucionComponent,
    SeMinminasComponent,
    SeMinminasCompromisosComponent,
    SeMinminasObligacionesComponent,
    SeAnhComponent,
    SeAnmComponent,
    SeCregComponent,
    SeIpseComponent,
    SeSgcComponent,
    SeUpmeComponent,
    SeAnhCompromisosComponent,
    SeAnhObligacionesComponent,
    SeAnmObligacionesComponent,
    SeAnmCompromisosComponent,
    SeCregCompromisosComponent,
    SeCregObligacionesComponent,
    SeIpseCompromisosComponent,
    SeIpseObligacionesComponent,
    SeSgcCompromisosComponent,
    SeSgcObligacionesComponent,
    SeUpmeCompromisosComponent,
    SeUpmeObligacionesComponent,
    CargarSiifComponent,
    CargarCdpComponent,
    ProyectosComponent,
    FileSelectDirective,
    ProyectoListComponent,
    ProyectoDetailComponent,
    PdTabDetailComponent,
    PdTabCompromisosComponent,
    PdTabObligacionesComponent,
    PdTabEjecucionComponent,
    CsListComponent,
    CsUploadComponent,
    EchartsDirective,
    MultiselectDirective,
    AlModalDirective,
    CompromisosEditComponent,
    ObligacionesEditComponent,
    TagsComponent,
    RankingEntidadesComponent,
    SeguimientoProyectoComponent,
    SeguimientoEntidadComponent,
    RoundNumberPipe,
    SectorComponent,
    ResumenCifrasComponent,
    ResumenEjecucionComponent,
    VariablesRelevantesComponent,
    ComportamientoEntidadesComponent,
    OrderByPipe,
    BtnMenuDirective,
    LoadingComponent,
    FormatNumberPipe,
    PresupuestoComponent,
    GraficoGeneralComponent,
    ProyectosDetalleComponent,
    ValidateUserComponent,
    DocumentosListComponent,
    DocumentosFormComponent,
    DocumentosComponent,
    CdpComponent,
    LiquidacionesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    routing,
    RlTagInputModule,
    Ng2Bs3ModalModule,
    PaginationModule,
    NgSelectizeModule
  ],
  providers: [
    PaginationConfig,
    ProyectoDataService,
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    LoadingService,
    MessageService,
    StorageManagerService,
    StateParamService],
  bootstrap: [AppComponent]
})

export class AppModule { }

