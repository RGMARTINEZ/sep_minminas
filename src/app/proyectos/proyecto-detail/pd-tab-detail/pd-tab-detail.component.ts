import { Component, OnInit } from '@angular/core';
import { Proyecto } from '../../../core/models/Proyecto';


@Component({
  selector: 'app-pd-tab-detail',
  templateUrl: './pd-tab-detail.component.html',
  styleUrls: ['./pd-tab-detail.component.less']
})
export class PdTabDetailComponent implements OnInit {

  proyecto : Proyecto;

  constructor() { }

  ngOnInit() {
  }

}
