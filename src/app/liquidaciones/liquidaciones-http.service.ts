import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { HttpService }  from '../core/services/http.service';
import { END_POINT } from '../globals';

@Injectable()
export class LiquidacionesHttpService extends HttpService {

  constructor (http: Http) {
     super(http);
   }

    login(user: string) {
      return this.get('Usuario/getRole?username=' + user);
    }

    validateToken(token: string, password: string) {
      return this.http.get(END_POINT + 'Usuario/confirmarRegistro?token=' + token + '&clave=' + password
    ).map(this._extractData).catch(this._handleError);
    }



    private _extractData(res: Response) {
      let body = res.json();
      return body || { };
    }


    private _handleError (error: Response | any) {
      let errMsg: string;
      if (error instanceof Response) {
        // 27-04-2017
        // Andres Lopera
        // Se valida cuando no está autorizado el ususario y es direccionado al login
        // borrando los datos de acceso del local storage.

        /*
        if (error.status == 401) {
          let router: Router;
          let storage : StorageManagerService = new StorageManagerService();

            storage.remove("ASDFKSLFIN442387KJ4I7487");
            storage.remove("DDDLDDLIEJU3934875723MDM");
            router.navigate(['/login']);

        };
        */
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      return Observable.throw(errMsg);
    }

}
