import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { HttpService } from '../core/services/http.service';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class CargarCdpHttpService extends HttpService {

   constructor (http: Http) {
     super(http);
   }

    url:String;

    

  getListSiif(): Observable<any> {
    return this.get("SIIF/getListRegistroSIIF");
  }



}
