import { TestBed, inject } from '@angular/core/testing';
import { PresupuestoHttpService } from './presupuesto-http.service';

describe('PresupuestoHttpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PresupuestoHttpService]
    });
  });

  it('should ...', inject([PresupuestoHttpService], (service: PresupuestoHttpService) => {
    expect(service).toBeTruthy();
  }));
});
