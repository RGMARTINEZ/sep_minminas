import { Injectable } from '@angular/core';

@Injectable()
export class UuidService {

  constructor() { }

    private s4() {
              return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
    }

    public newId() {
      return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' +
      this.s4() + '-' + this.s4() + this.s4() + this.s4();
    }


}
