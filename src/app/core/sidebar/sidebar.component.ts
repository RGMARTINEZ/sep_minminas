import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageManagerService } from '../services/storage-manager.service';
import { StateParamService } from '../services/state-param.service';
import { STATE_PARAM_GLOBAL } from '../../globals';
import { GlobalVariablesService } from '../services/global-variables.service';
import { RouterChangedService } from './router-changed.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.less']
})


export class SidebarComponent implements OnInit {
  menu: any;
  date: any;
  month: any;


  goToRoute(link, _tags) {

    this.date = new Date();
    this.month = this.date.getMonth();
    this.month = (this.month === 1 ) ? this.month : this.month;

    let _param = {
      entidad: 'TODOS',
      mes: this.month,
      tipoPresupuesto : 'TODOS'
    };

    const paramStorage = this.stateParamService.getParams(STATE_PARAM_GLOBAL);
    _param = (paramStorage != null) ? paramStorage : _param;

    const _tmp = [];
    if (_tags.length > 0) {
      for (let i = 0; i < _tags.length ; i++) {
        _tmp.push(_tags[i].nombreTag);
      }
      _param['tags'] = _tmp;
    }

    this.stateParamService.setParams( STATE_PARAM_GLOBAL , _param);
    ( this.router.url === link ) ? this.routerChangedService.setRoute(link) : this.router.navigate([link]);

  }

  logOut() {
    this.storageManagerService.remove('ASDFKSLFIN442387KJ4I7487');
    this.storageManagerService.remove('DDDLDDLIEJU3934875723MDM');
    this.router.navigate(['/login']);
  }



  procesarMenu(data) {
    let opciones = data[0].opciones;
    let menu: Array<any> = new Array<any>();
    for (let i = 0; i < opciones.length ; i++ ) {
      if (opciones[i].idParent == null) {
        let _primerNiver = opciones[i];
        _primerNiver['opciones'] = [];
        menu.push(_primerNiver);
      }
    }

    for (let i = 0; i < opciones.length; i++ ) {
      for (let k = 0; k < menu.length ; k++) {
        if (opciones[i].idParent === menu[k].idOpcion ) {
          menu[k].opciones.push(opciones[i]);
          break;
        }
      }
    }
    return menu;
  }

  constructor(private router: Router,
  private storageManagerService: StorageManagerService,
  private stateParamService: StateParamService,
  private globalVariablesService: GlobalVariablesService,
  private routerChangedService: RouterChangedService) {}

  ngOnInit() {
    let _menu = atob( this.storageManagerService.retrieve('laksdfioeLIDLASKS') );
    this.menu = this.procesarMenu(JSON.parse(_menu) );
  }

}
