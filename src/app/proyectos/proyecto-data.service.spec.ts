/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProyectoDataService } from './proyecto-data.service';

describe('Service: ProyectoData', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProyectoDataService]
    });
  });

  it('should ...', inject([ProyectoDataService], (service: ProyectoDataService) => {
    expect(service).toBeTruthy();
  }));
});
