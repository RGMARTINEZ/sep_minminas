import { TestBed, inject } from '@angular/core/testing';
import { StateParamService } from './state-param.service';

describe('StateParamService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StateParamService]
    });
  });

  it('should ...', inject([StateParamService], (service: StateParamService) => {
    expect(service).toBeTruthy();
  }));
});
