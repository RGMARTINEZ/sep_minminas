import { Component, ViewChild, OnInit } from '@angular/core';
import { EchartsDirective } from '../../../core/directives/echarts.directive';
import { SeguimientoEjecucionDataService } from '../../seguimiento-ejecucion-data.service'; 
import { ProcesarObligaciones } from '../../model/ProcesarObligaciones';

@Component({
  selector: 'app-se-upme-obligaciones',
  templateUrl: './se-upme-obligaciones.component.html',
  styleUrls: ['./se-upme-obligaciones.component.less']
})
export class SeUpmeObligacionesComponent extends ProcesarObligaciones implements OnInit {


  @ViewChild(EchartsDirective) echart: EchartsDirective;
  public echartOption: any = {};
  
  setDataToChart(options) {
     this.echart.updateData(options);
  }

  goToProjects() {
    this.sData.goToProject(this.UEJ);
  }
  
  constructor(private sData : SeguimientoEjecucionDataService) {
    super();
    this.UEJ = "21-09-00";
    this.sData.dataGraficos.subscribe(data => {
      this.updateData(data);
    });
        this.sData.dataDatos.subscribe(entidades => {
      this.getDataEntidad(entidades)
    })

  }


  ngOnInit() {
  }
}
