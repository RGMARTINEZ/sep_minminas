import { DocumentosFormComponent } from './documentos/documentos-form/documentos-form.component';
import { ValidateUserComponent } from './validate-user/validate-user.component';
import { PresupuestoComponent } from './presupuesto/presupuesto.component';
import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { SeguimientoEjecucionComponent } from './seguimiento-ejecucion/seguimiento-ejecucion.component';
import { LoginComponent } from './login/login.component';
import { CargarSiifComponent } from './cargar-siif/cargar-siif.component';

import { CargarCdpComponent } from './cargar-cdp/cargar-cdp.component';


import { ProyectosComponent } from './proyectos/proyectos.component';
import { ProyectoListComponent } from './proyectos/proyecto-list/proyecto-list.component';
import { ProyectoDetailComponent } from './proyectos/proyecto-detail/proyecto-detail.component';
import { CsListComponent } from './cargar-siif/cs-list/cs-list.component';
import { CsUploadComponent } from './cargar-siif/cs-upload/cs-upload.component';
import { RankingEntidadesComponent } from './ranking-entidades/ranking-entidades.component';
import { SeguimientoProyectoComponent } from './seguimiento-proyecto/seguimiento-proyecto.component';
import { SeguimientoEntidadComponent } from './seguimiento-entidad/seguimiento-entidad.component';
import { SectorComponent } from './sector/sector.component';
import { DocumentosComponent } from './documentos/documentos.component';
import { DocumentosListComponent } from './documentos/documentos-list/documentos-list.component';

import { CdpComponent } from './cdp/cdp.component';
import { LiquidacionesComponent } from './liquidaciones/liquidaciones.component';





const appRoutes: Routes = [{
       path: '',
        redirectTo: 'login',
        pathMatch: 'full'
  }, {
    path: 'app',
    component: MainComponent,
    children: [{
      path: 'ingresoCdp',
      component: CdpComponent
    },
    {
      path: 'liquidaciones',
      component: LiquidacionesComponent
    },
    {
      path: 'sector',
      component: SectorComponent
    }, {
      path: 'seguimiento',
      component: SeguimientoEjecucionComponent
    }, {
      path: 'seguimientoEntidad',
      component: SeguimientoEntidadComponent
    }, {
      path: 'seguimientoProyecto',
      component: SeguimientoProyectoComponent
    }, {
      path: 'rankingEntidades',
      component: RankingEntidadesComponent
    }, {
      path: 'archivoSiif',
      component: CargarSiifComponent,
      children: [{
        path: '',
        component: CsListComponent
      }, {
        path: 'cargar',
        component: CsUploadComponent
      }]
    }, 
    {
      path: 'cargueCdp',
      component: CargarCdpComponent,
      children: [{
        path: '',
        component: CsListComponent
      }, {
        path: 'cargar',
        component: CsUploadComponent
      }]
    },{
      path: 'proyectos',
      component: ProyectosComponent,
      children : [{
        path: '',
        component: ProyectoListComponent
      }, {
        path: 'proyecto',
        component: ProyectoDetailComponent
      }]
    }, {
      path: 'presupuesto',
      component: PresupuestoComponent
    }, {
      path: 'documentos',
      component: DocumentosComponent,
      children: [ { path: '',
        component: DocumentosListComponent
      }, {
        path: 'nuevo',
      component: DocumentosFormComponent
    }]
    }]
  }, {
    path: 'login',
    component: LoginComponent
  }, {
    path: 'validateUser/:token',
    component: ValidateUserComponent
  }];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);