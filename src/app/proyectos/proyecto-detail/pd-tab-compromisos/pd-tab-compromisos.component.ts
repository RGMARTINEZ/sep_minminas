import { Component, ViewChild, OnInit } from '@angular/core';
import { Compromiso } from '../../../core/models/Compromiso';
import { Actividad } from '../../../core/models/Actividad';
import { CompromisosEditComponent } from '../compromisos-edit/compromisos-edit.component';
import { ActividadResumen } from '../../../core/models/ActividadResumen';

@Component({
  selector: 'app-pd-tab-compromisos',
  templateUrl: './pd-tab-compromisos.component.html',
  styleUrls: ['./pd-tab-compromisos.component.less']
})
export class PdTabCompromisosComponent implements OnInit {

  @ViewChild(CompromisosEditComponent) compromisosEdit: CompromisosEditComponent;
  public actividades : Array<Actividad>;
  public actividades_compromisos : Array<any> = new Array<any>();
  public total_mes : Array<number> = new Array<number>();
  public total_compromisos : number = 0;
  private actividadResumen : ActividadResumen = new ActividadResumen();
  
  public changeCompromiso(event) {
    this.updateCompromisos();
  }

  public editActividad(actividad: ActividadResumen) {
    this.compromisosEdit.setActividad(actividad);
  }

  private obtenerActividad(idActividad : number) {
    for (var i = 0; i < this.actividades.length; i++) {
      if (this.actividades[i].idActividad == idActividad) {
        return this.actividades[i];
      }
    }
  }

  private obtenerCompromiso(idCompromiso: number, actividad : Actividad) {
    var result = null;
    for (var i = 0 ; actividad.compromiso.length; i++) {
      if (idCompromiso == actividad.compromiso[i].idCompromiso) {
        result = actividad.compromiso[i];
        break;
      }
    }
    return result;
  }


  public getCompromisos() {
    var result : Array<Actividad> = new Array<Actividad>();
    for (var i = 0; i < this.actividades_compromisos.length; i++) {
      var tmp_actividad : Actividad = this.obtenerActividad( this.actividades_compromisos[i].idActividad);
      var tmp_compromisos : Array<Compromiso> = new Array<Compromiso>();

      for (var j=0; j < this.actividades_compromisos[i].compromisos.length; j++) {
        var tmp_compromiso : Compromiso = new Compromiso();
        if (this.actividades_compromisos[i].compromisos[j].id != null) {
            tmp_compromiso = this.obtenerCompromiso(this.actividades_compromisos[i].compromisos[j].id, tmp_actividad);
        } else {
          tmp_compromiso.mes = j + 1;
        }
        tmp_compromiso.valor = this.actividades_compromisos[i].compromisos[j].valor;
        (tmp_compromiso.valor > 0) ? tmp_compromisos.push(tmp_compromiso) : null;
      }

      tmp_actividad.compromiso = tmp_compromisos;
      result.push(tmp_actividad);
    }
    return result;
  }

  constructor() {
    this.actividadResumen = new ActividadResumen();
    for (var i = 0; i < 12; i++) {
      this.total_mes.push(0);
    }
  }

  public CargarActividades(actividades : Array<Actividad>) {
    this.actividades = actividades;
    let compromiso: any;
    for (var i = 0; i < actividades.length; i++) {
      this.actividadResumen = new ActividadResumen();
      this.actividadResumen.idActividad = actividades[i].idActividad;
      this.actividadResumen.nombreActividad = actividades[i].nombreActividad;
      for (var j = 0; j < actividades[i].compromiso.length; j++) {
        compromiso = actividades[i].compromiso[j];
        this.actividadResumen.compromisos[compromiso.mes - 1].id = compromiso.idCompromiso;
        this.actividadResumen.compromisos[compromiso.mes - 1].valor = compromiso.valor;
        this.actividadResumen.total += compromiso.valor;
        this.total_mes[compromiso.mes - 1] = this.total_mes[compromiso.mes - 1] + compromiso.valor;
        this.total_compromisos += compromiso.valor; 
      }
      this.actividades_compromisos.push(this.actividadResumen);
    }
  }


  public updateCompromisos() {
    this.total_compromisos = 0;
    this.total_mes = [];
    for (var i = 0; i < 12; i++) {
      this.total_mes.push(0);
    }
    for (var i = 0; i < this.actividades_compromisos.length; i++) {
        this.actividades_compromisos[i].total = Number(0);
      for (var j = 0; j < this.actividades_compromisos[i].compromisos.length; j++) {
        this.actividades_compromisos[i].total += Number(this.actividades_compromisos[i].compromisos[j].valor);        
        this.total_mes[j] += Number(this.actividades_compromisos[i].compromisos[j].valor);
        this.total_compromisos += Number(this.actividades_compromisos[i].compromisos[j].valor);
      }
    }
  }



  ngOnInit() {
    this.CargarActividades(this.actividades);
  }

}
