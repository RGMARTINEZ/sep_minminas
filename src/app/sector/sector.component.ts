import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { SectorDataService } from './sector-data.service';
import { MessageService } from '../core/services/message.service';
import { StateParamService } from '../core/services/state-param.service';
import { STATE_PARAM_GLOBAL } from '../globals';
import { GlobalVariablesService } from '../core/services/global-variables.service';

@Component({
  selector: 'app-sector',
  templateUrl: './sector.component.html',
  styleUrls: ['./sector.component.css'],
  providers : [SectorDataService]
})
export class SectorComponent implements OnInit {

  anio : number = new Date ().getFullYear();
  mes : string;
  sub : any;
  result : any;


  setData() {
  }

  obtenerDatos() {
    if (this.anio != null  && this.mes != null) {
       var _param = {
        entidad: this.result.entidad, 
        mes: this.mes,
        tipoPresupuesto : this.result.tipoPresupuesto,
        tags : this.result.tags
      }
      this.stateParamService.setParams( STATE_PARAM_GLOBAL , _param );
      this.sectorDataService.setData({ mes : this.mes, anio : this.anio })
    } else {
      this.messageService.showInfo(null, "debe ingresar el año y el mes para consultar")
    }
  }

  goToProject(uej : string) {
     var _param = {
        entidad: uej, 
        mes: this.mes,
        tipoPresupuesto : this.result.tipoPresupuesto,
        tags : []
      }
      this.stateParamService.setParams( STATE_PARAM_GLOBAL , _param );
    this.route.navigate(['app/seguimientoProyecto']);
  }

  goToSeguimiento(uej : string) {
     var _param = {
        entidad: uej, 
        mes: this.mes,
        tipoPresupuesto : this.result.tipoPresupuesto,
        tags : []
      }
      this.stateParamService.setParams( STATE_PARAM_GLOBAL , _param );
    this.route.navigate(['app/seguimiento']);
  }

  constructor(private sectorDataService :SectorDataService,
  private messageService : MessageService, 
  private route: Router,
  private stateParamService: StateParamService,
  private globalVariablesService : GlobalVariablesService) { }


  ngOnInit() {
    this.result = this.stateParamService.getParams(STATE_PARAM_GLOBAL)
    if (this.result != null) {
      this.mes = this.result.mes;
      this.obtenerDatos();
    }
  }

}
