import { Component, ViewChild, OnInit } from '@angular/core';
import { EchartsDirective } from '../../../core/directives/echarts.directive';
import { SeguimientoEjecucionDataService } from '../../seguimiento-ejecucion-data.service'; 
import { ProcesarObligaciones } from '../../model/ProcesarObligaciones';

@Component({
  selector: 'app-se-creg-obligaciones',
  templateUrl: './se-creg-obligaciones.component.html',
  styleUrls: ['./se-creg-obligaciones.component.less']
})
export class SeCregObligacionesComponent extends ProcesarObligaciones implements OnInit {

  @ViewChild(EchartsDirective) echart: EchartsDirective;
  public echartOption: any = {};
  
   setDataToChart(options) {
     this.echart.updateData(options);
  }

  goToProjects() {
    this.sData.goToProject(this.UEJ);
  }
  
  constructor(private sData : SeguimientoEjecucionDataService) {
    super();
    this.UEJ = "21-01-13";
    this.sData.dataGraficos.subscribe(data => {
      this.updateData(data);
    });

        this.sData.dataDatos.subscribe(entidades => {
      this.getDataEntidad(entidades)
    })

  }



  ngOnInit() {
  }
}
