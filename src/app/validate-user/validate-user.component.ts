import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LoginHttpService } from '../login/login-http.service';
import { MessageService } from '../core/services/message.service';
import { LoadingService } from '../core/loading/loading.service';
import { StateParamService } from '../core/services/state-param.service';

@Component({
  selector: 'app-validate-user',
  templateUrl: './validate-user.component.html',
  styleUrls: ['./validate-user.component.css'],
  providers: [LoginHttpService]
})
export class ValidateUserComponent implements OnInit {


  password: string = null;
  repeat_password: string = null;
  token: string;
  private subRouter: any;

  constructor(private loginHttpService: LoginHttpService,
      private messageService: MessageService,
      private loadingService: LoadingService,
      private router: Router,
      private activeRoute: ActivatedRoute,
      private stateParamService: StateParamService) {

 }

procesarResult(result) {
  this.messageService.showSuccess(null, 'Usuario activado.');
  this.router.navigate(['login']);
  this.loadingService.setLoading(false);
  /*
  let _menu = JSON.stringify(result);
  this.storageManagerService.store("laksdfioeLIDLASKS", btoa(_menu));
  this.loadingService.setLoading(false);
  let state = ( result[0].name == 'DIRECTIVOS' ||  result[0].name == 'ADMIN' ) ? 'app/sector' : 'app/seguimiento';
  this.date = new Date();
  this.month = this.date.getMonth();
  this.month = (this.month == 0 ) ? this.month : this.month;

  var _param = {
    entidad: "TODOS",
    mes: this.month,
    tipoPresupuesto : "TODOS"
  }

  this.stateParamService.setParams( STATE_PARAM_GLOBAL , _param );
  this.router.navigate([state]);
  */
}

setError(error) {
  this.messageService.showError(null, 'Token inválido, contáctese con el administrador.');
  this.loadingService.setLoading(false);
}

validateToken() {
  if (this.password === this.repeat_password) {
    this.loadingService.setLoading(true);
    this.loginHttpService.validateToken(this.token, this.password).subscribe(
      result => this.procesarResult(result),
     error => this.setError(error));
  } else {
    this.messageService.showError(null, 'No coinciden las claves asignadas');
  }
}

ngOnInit() {

  this.subRouter = this.activeRoute.params.subscribe(params => {
    this.token = params['token'];
  });
}

}
