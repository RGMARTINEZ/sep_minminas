import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { HttpService } from '../core/services/http.service';


// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';



@Injectable()
export class TagsHttpService extends HttpService {

   constructor (http: Http) {
     super(http);
   }

  getTags(): Observable<any> {
    return this.get("Tag");
  }

}
