export class ProyectoRecurso {

    idProyectoRecurso: number;
    aprinicial: number;
    apradicionada: number;
    aprreducida: number;
    aprvigente: number;
    aprbloqueada: number;
    cdp: number;
    aprdisponible: number;
    compromiso: number;
    obligacion: number;
    ordenpago: number;
    pagos: number;
    idRegistro: number;

}