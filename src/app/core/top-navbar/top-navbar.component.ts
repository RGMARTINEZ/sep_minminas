import { Component, OnInit } from '@angular/core';
import { TopNavbarService } from './top-navbar.service';


@Component({
  selector: 'app-top-navbar',
  templateUrl: './top-navbar.component.html',
  styleUrls: ['./top-navbar.component.less']
})
export class TopNavbarComponent implements OnInit {

  lShowMenu: boolean = true;

  public changeMenuStatus(event) {
    this.lShowMenu = !this.lShowMenu;
    this.topNavbarService.changeNav(this.lShowMenu);
  }
  
  constructor(private topNavbarService: TopNavbarService) {
    topNavbarService.changeNav(this.lShowMenu);
  }

  ngOnInit() {
  }

}
