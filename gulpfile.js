var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');
var sass = require('gulp-sass');

gulp.task('less', function () {
  return gulp.src('./src/less/*.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./src/assets/styles'));
});


gulp.task('sass', function () {
  return gulp.src('./src/less/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./src/assets/styles'));
});