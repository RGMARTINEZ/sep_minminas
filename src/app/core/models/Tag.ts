export class Tag {
    idTag: number;
    nombreTag: string;

    constructor(idTag : number, nombreTag : string) {
        this.idTag= idTag;
        this.nombreTag = nombreTag;
    }
}