import { Directive, ElementRef,  Renderer, AfterViewInit } from '@angular/core';
declare var jQuery: any;

@Directive({
  selector: '[appBtnMenu]'
})
export class BtnMenuDirective {
  
    constructor(private el: ElementRef, private renderer: Renderer) { }

    ngAfterViewInit() {
      jQuery(this.el.nativeElement).sideNav({
      menuWidth: 300, // Default is 300
      edge: 'left', // Choose the horizontal origin
      closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
      draggable: true // Choose whether you can drag to open on touch screens
    })

  }
  
  

}
