/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SeguimientoEjecucionDataService } from './seguimiento-ejecucion-data.service';

describe('Service: SeguimientoEjecucionData', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SeguimientoEjecucionDataService]
    });
  });

  it('should ...', inject([SeguimientoEjecucionDataService], (service: SeguimientoEjecucionDataService) => {
    expect(service).toBeTruthy();
  }));
});
