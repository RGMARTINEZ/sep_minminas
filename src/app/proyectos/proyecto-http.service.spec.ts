/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProyectoHttpService } from './proyecto-http.service';

describe('Service: ProyectoHttp', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProyectoHttpService]
    });
  });

  it('should ...', inject([ProyectoHttpService], (service: ProyectoHttpService) => {
    expect(service).toBeTruthy();
  }));
});
