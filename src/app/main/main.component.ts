import { Component, OnInit } from '@angular/core';
import { TopNavbarService } from '../core/top-navbar/top-navbar.service';
import {Subscription} from 'rxjs/Subscription';
import { GlobalVariablesService } from '../core/services/global-variables.service';
import { RouterChangedService } from '../core/sidebar/router-changed.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.less'],
  providers: [TopNavbarService, GlobalVariablesService, RouterChangedService]
})
export class MainComponent implements OnInit {

  showMenu : boolean;
  subscription:Subscription;
  constructor(private topNavbarService: TopNavbarService) {}

  ngOnInit() {
    this.subscription = this.topNavbarService.showMenu.subscribe(
      showMenu => this.showMenu = showMenu
    );
  }
  ngOnDestroy() {
    // prevent memory leak when component is destroyed
    this.subscription.unsubscribe();
  }

}
