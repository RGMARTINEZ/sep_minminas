import { Component, OnInit, Input } from '@angular/core';
import { SeguimientoEjecucionDataService } from '../seguimiento-ejecucion-data.service';

@Component({
  selector: 'app-se-upme',
  templateUrl: './se-upme.component.html',
  styleUrls: ['./se-upme.component.less']
})
export class SeUpmeComponent implements OnInit {
  @Input()
  set entidadHidden(entidades : any) {
    (entidades.length == 0) ? this.visible = true: null;
    for (var i = 0; i < entidades.length; i++) {
      (entidades[i] == "21-09-00") ? this.visible = false: null; 
    }
  }

  visible : boolean;

  ngOnInit() {
  }

  isVisible(entidad) {
    this.visible = (entidad == "21-09-00" || entidad == "TODOS") ? true : false;
  }



    constructor(private sData : SeguimientoEjecucionDataService) {
    this.sData.showEntidad.subscribe(entidad => {
      this.isVisible(entidad);
    });
  }
}
