import { Component, ViewChild, OnInit } from '@angular/core';
import { EchartsDirective } from '../../../core/directives/echarts.directive';
import { SeguimientoEjecucionDataService } from '../../seguimiento-ejecucion-data.service'; 
import {ProcesarCompromisos } from '../../model/ProcesarCompromisos';

@Component({
  selector: 'app-se-ipse-compromisos',
  templateUrl: './se-ipse-compromisos.component.html',
  styleUrls: ['./se-ipse-compromisos.component.less']
})
export class SeIpseCompromisosComponent extends ProcesarCompromisos implements OnInit {

  @ViewChild(EchartsDirective) echart: EchartsDirective;
  public echartOption: any = {};

  setDataToChart(options) {
    this.echart.updateData(options);
  }

    goToProjects() {
    this.sData.goToProject(this.UEJ);
  }


  constructor(private sData : SeguimientoEjecucionDataService) {
    super();
    this.UEJ = "21-10-00";
    this.sData.dataGraficos.subscribe(data => {
      this.updateData(data);
    });

    this.sData.dataDatos.subscribe(entidades => {
      this.getDataEntidad(entidades)
    })

  }


  ngOnInit() {
  }

}
