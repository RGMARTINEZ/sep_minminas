import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';


@Injectable()
export class SeguimientoEjecucionDataService {

  public dataGraficos: Subject<any> = new Subject();
  public dataDatos: Subject<any> = new Subject();
  public showEntidad: Subject<string> = new Subject();
  public goProject: Subject<any> = new Subject();

  public setDataGraficos(data) {
    this.dataGraficos.next(data);
  }

  public setDataDatos(data) {
    this.dataDatos.next(data);
  }

  public setShowEntidad(entidad) {
    this.showEntidad.next(entidad);
  }

  public goToProject(uej : string) {
    this.goProject.next(uej);
  }

  constructor() {
  }

}
