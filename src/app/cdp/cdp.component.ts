import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { CdpHttpService } from './cdp-http.service';
import { MessageService } from '../core/services/message.service';
import { LoadingService } from '../core/loading/loading.service';
import { StorageManagerService } from '../core/services/storage-manager.service';

import { StateParamService } from '../core/services/state-param.service';
import { STATE_PARAM_GLOBAL } from '../globals';


@Component({
  selector: 'app-cdp',
  templateUrl: './cdp.component.html',
  styleUrls: ['./cdp.component.less'],
  providers: [CdpHttpService]
})
export class CdpComponent implements OnInit {

    user : string = null;
    password : string = null;
    date : any;
    month : any;

    constructor(private cdpHttpService: CdpHttpService,
        private messageService : MessageService,
        private loadingService: LoadingService,
        private storageManagerService: StorageManagerService,
        private router : Router,
        private stateParamService : StateParamService) {

   }

  procesarResult(result) {
    let _menu = JSON.stringify(result);
    this.storageManagerService.store('laksdfioeLIDLASKS', btoa(_menu));
    this.loadingService.setLoading(false);
    let state = ( result[0].name == 'DIRECTIVOS' ||  result[0].name == 'ADMIN' ) ? 'app/sector' : 'app/seguimiento';
    this.date = new Date();
    this.month = this.date.getMonth();
    this.month = (this.month == 0 ) ? this.month : this.month;

    const _param = {
      entidad: 'TODOS',
      mes: this.month,
      tipoPresupuesto : 'TODOS'
    }

    this.stateParamService.setParams( STATE_PARAM_GLOBAL , _param );
    this.router.navigate([state]);
//    this.router.navigate([state],  { queryParams: { e: "TODOS", m: this.month, tp : "TODOS" } });
  }

  setError(error) {
    this.messageService.showError(null, 'Usuario o Clave invalida, puede ser que no está autorizado para ingresar a la aplicación.')
    this.loadingService.setLoading(false)
  }

  validateLogin() {
    if (this.user == null || this.password == null) {
      this.messageService.showWarning(null,'Debe ingresar usuario y contraseña.')
    } else {
      this.user = this.user.toLowerCase();
      this.storageManagerService.store('ASDFKSLFIN442387KJ4I7487', btoa(this.user));
      this.storageManagerService.store('DDDLDDLIEJU3934875723MDM', btoa(this.password));
      this.loadingService.setLoading(true)
      this.cdpHttpService.login(this.user.toLowerCase()).subscribe(result => this.procesarResult(result),
          error => this.setError(error));
    }
  }


  validateLoginInit() {
   let user = this.storageManagerService.retrieve('ASDFKSLFIN442387KJ4I7487');
   let password = this.storageManagerService.retrieve('DDDLDDLIEJU3934875723MDM');

    if (user !== '' && password !== '') {
      this.loadingService.setLoading(true);
      this.cdpHttpService.login( atob(user) ).subscribe(result => this.procesarResult(result),
            error => this.loadingService.setLoading(false));
    }

  }

  ngOnInit() {
    //this.validateLoginInit();
  }

}
