import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VariablesRelevantesComponent } from './variables-relevantes.component';

describe('VariablesRelevantesComponent', () => {
  let component: VariablesRelevantesComponent;
  let fixture: ComponentFixture<VariablesRelevantesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VariablesRelevantesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariablesRelevantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
