import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class RouterChangedService {
  public onChanged : Subject<any> = new Subject();

  constructor() { }

  public setRoute(url : string) {
    this.onChanged.next(url);
  }

}
