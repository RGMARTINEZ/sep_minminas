import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingService } from './loading.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements OnInit {
  isLoading : boolean = false;

  backToHome() {
    this.isLoading = false;
  this.router.navigateByUrl('/');
  }

  constructor(private loadingService : LoadingService, private router : Router) { }

  ngOnInit() {

    this.loadingService.isLoading.subscribe(isLoading => {
      this.isLoading = isLoading;
    })
  }

}
