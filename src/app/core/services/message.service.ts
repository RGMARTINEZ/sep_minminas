import { Injectable } from '@angular/core';
declare var toastr: any;
// https://www.npmjs.com/package/ng2-toastr

@Injectable()
export class MessageService {

    constructor() {
      toastr.options.closeButton = (true);
      toastr.options.positionClass = 'toast-bottom-right';
      toastr.options.progressBar = true;
      toastr.options.preventDuplicates = true;
      }

      showSuccess(title: string, message: string) {
        toastr['success'](message, title);
      }

      showError(title: string, message: string) {
      toastr['error'](message, title);
      }

      showWarning(title: string, message: string) {
      toastr['warning'](message, title);
      }

      showInfo(title: string, message: string) {
      toastr['info'](message, title);
      }

}
