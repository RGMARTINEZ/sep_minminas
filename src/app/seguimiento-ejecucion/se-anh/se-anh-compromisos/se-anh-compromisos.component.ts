import { Component, ViewChild, OnInit, Output, EventEmitter } from '@angular/core';

import { EchartsDirective } from '../../../core/directives/echarts.directive';
import { SeguimientoEjecucionDataService } from '../../seguimiento-ejecucion-data.service'; 
import {ProcesarCompromisos } from '../../model/ProcesarCompromisos';

@Component({
  selector: 'app-se-anh-compromisos',
  templateUrl: './se-anh-compromisos.component.html',
  styleUrls: ['./se-anh-compromisos.component.less']
})
export class SeAnhCompromisosComponent extends ProcesarCompromisos implements OnInit {

  @ViewChild(EchartsDirective) echart: EchartsDirective;
  @Output() onGoToProjects = new EventEmitter();

  public echartOption: any = {};

  setDataToChart(options) {
    this.echart.updateData(options);
  }

  goToProjects() {
    this.sData.goToProject(this.UEJ);
  }


  constructor(private sData : SeguimientoEjecucionDataService) {
    super();
    this.UEJ = "21-11-00";
    this.sData.dataGraficos.subscribe(data => {
      this.updateData(data);
    });

    this.sData.dataDatos.subscribe(entidades => {
      this.getDataEntidad(entidades)
    })
  }

  ngOnInit() {
  }

}
