import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { HttpService } from '../core/services/http.service';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';



@Injectable()
export class PresupuestoHttpService extends HttpService {

   constructor (http: Http) {
     super(http);
   }

  getPresupuesto(): Observable<any> {
    return this.get('Presupuesto');
  }

}
