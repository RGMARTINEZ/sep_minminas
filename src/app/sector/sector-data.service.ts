import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class SectorDataService {

  public data : Subject<any> = new Subject();
  public _data : any;


  public setData(data) {
    this._data = data;
    this.data.next(data);
  }

  constructor() { 
  }


  
}
