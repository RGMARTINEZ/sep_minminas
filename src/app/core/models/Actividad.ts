import { Compromiso } from './Compromiso';
import { Obligacion } from './Obligacion';

export class Actividad {

    idActividad: number;
    nombreActividad: string;
    producto: string;
    meta: string;
    cpc: string;
    etapa: string;
    objetivo: string;
    ciiu: string;
    valorIndicativo: number;
    valorSolicitado: number;
    valorVigente: number;
    idRegistroSuifp: number;
    bpin: string;
    uej: string;
    rubro: string;
    anio: number;
    compromiso: Array<Compromiso>;
    obligacion: Array<Obligacion>;

}