import { Directive, ElementRef,  Renderer, AfterViewInit } from '@angular/core';
declare var jQuery: any;


@Directive({
  selector: '[appMultiselect]'
})
export class MultiselectDirective {



  constructor(private el: ElementRef, private renderer: Renderer) {
  }

  ngAfterViewInit() {
    jQuery(this.el).selectpicker({
      style: 'btn-info',
      size: 4
    });
  }



}
