/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { ProyectoListComponent } from './proyecto-list.component';
import { Router } from '@angular/router';
import { ProyectoHttpService } from '../proyecto-http.service';
import { ProyectoDataService } from '../proyecto-data.service';

describe('Component: ProyectoList', () => {
  it('should create an instance', () => {
    let proyectoHttpService: ProyectoHttpService;
    let proyectoDataService : ProyectoDataService;
    let router: Router;

    let component = new ProyectoListComponent(proyectoHttpService, proyectoDataService, router);
    expect(component).toBeTruthy();
  });
});
