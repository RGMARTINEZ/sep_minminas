/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CargarSiifHttpService } from './cargar-siif-http.service';

describe('Service: CargarSiifHttp', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CargarSiifHttpService]
    });
  });

  it('should ...', inject([CargarSiifHttpService], (service: CargarSiifHttpService) => {
    expect(service).toBeTruthy();
  }));
});
