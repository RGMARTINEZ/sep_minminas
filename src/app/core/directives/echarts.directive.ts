import { AfterContentInit, ChangeDetectorRef, Directive, ElementRef, Input, OnInit, Renderer } from '@angular/core';
import * as echarts from 'echarts/dist/echarts';
import { TopNavbarService } from '../top-navbar/top-navbar.service';
import { Subscription } from 'rxjs/Subscription';


@Directive({
  selector: '[app-echarts]'
})
export class EchartsDirective implements AfterContentInit {

  innerWidth: number;
  private myChart;
  private window: Window;
  subscription: Subscription;
  private listenerEcharts;
  public isinit: Boolean = false;


  @Input() echartOptions: any;

  private options: any = {
    title: {
      show: false
    },
    grid: {
    x : 30,
    y : 20,
    x2: 10,
    y2: 45
    },
    tooltip: {
      trigger: "item",
      formatter: "{a} <br/>{b} : {c}"
    },
    legend: {
      show: false
    },
    symbolSize : 10,
    xAxis: [
      {
        type: 'category',
        name: '',
        splitLine: { show: false },
        axisLabel: { rotate: 90, textStyle : { fontSize: 9 }
         },
        data: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
//        data: [1,2,3,4,5,6,7,8,9,10,11,12]
      }
    ],
    yAxis: [
      {
        type: "value",
        axisLabel: {
            formatter: '{value}%',
            textStyle : { fontSize: 9 }
        }
      }
    ],
    series : [{
        name: 'planeado',
        type: 'line',
        itemStyle: {normal: {borderWidth : 9}},
        smooth: true,
        data : []
    }, {
        name: 'ejecutado',
        type: 'line',
        itemStyle: {normal: {borderWidth : 9}},
        smooth: true,
        data : []
    }],
    toolbox: {
      show: false,
    },
    calculable: false
  };
// 0,0,0,0,0,0,0,0,0,0,0,0

  public updateData(options: any) {
    /*
    var _options = this.myChart.getOption();
    _options.series[0].data = options.series[0].data;
    _options.series[1].data = options.series[1].data;
    */
    this.myChart.setOption(options);
//    this.myChart.resize();
  }

  constructor(private el: ElementRef, private renderer: Renderer, private cdr: ChangeDetectorRef, private topNavbarService: TopNavbarService) {

    window.onresize = () => {
      this.myChart.resize();
      this.cdr.detectChanges(); //running change detection manually
    };

    this.renderer.setElementStyle(this.el.nativeElement, 'width', '99%');
    this.renderer.setElementStyle(this.el.nativeElement, 'height', '99%');
    this.subscription = this.topNavbarService.showMenu.subscribe(
    );
  }

  ngAfterContentInit() {
    this.initEchart();
  }

  initEchart() {
    if (!this.isinit) {
      Object.assign(this.options, this.echartOptions);
      this.myChart = echarts.init(this.el.nativeElement);
      this.myChart.setOption(this.options);
      this.isinit = true;
    }
  }

}
