import { Component, ViewChild, OnInit } from '@angular/core';
import { EchartsDirective } from '../../core/directives/echarts.directive';
import { ProyectoHttpService } from '../../proyectos/proyecto-http.service';
import { SectorDataService } from '../sector-data.service';

@Component({
  selector: 'app-resumen-ejecucion',
  templateUrl: './resumen-ejecucion.component.html',
  styleUrls: ['./resumen-ejecucion.component.css'],
  providers: [ProyectoHttpService]
})
export class ResumenEjecucionComponent implements OnInit {
  @ViewChild(EchartsDirective) echart: EchartsDirective;  
  public echartOption: any = {};


  anio : number;
  mes : number;

  loading : boolean = false;
  noData : boolean = true;
  error : boolean = false;
  noSIIFLoader : boolean = false;



  public updateData(data) {
    let markPointMes = this.mes - 1;
    let markPointValue = 0;
    let markPointColor = "";

        this.loading = false;
        this.noData = (data.lenght == 0) ? true : false;
        this.error = false;
        this.noSIIFLoader = false;


    let mejorAnio = [];
    let proyectadoPresidencia = [];
    let ejecutado = [];



    for (var i = 0; i < data.length ; i++) {
      let porcentaje_obligado = null;
      let porcentaje_planeado = null;
      mejorAnio.push( (data[i].mejorAnio== 0) ? null : data[i].mejorAnio );
      porcentaje_planeado = (data[i].proyectadoPresidencia == 0) ? null : data[i].proyectadoPresidencia
      proyectadoPresidencia.push( porcentaje_planeado );
      porcentaje_obligado = (data[i].obligado == 0 || data[i].obligado == null || data[i].apropiacionDisponible == null || data[i].apropiacionDisponible == 0) ? null :  parseFloat(data[i].obligado / data[i].apropiacionDisponible * 100+ "").toFixed(2);
      ejecutado.push(porcentaje_obligado);

           if (i == markPointMes) {
              markPointValue = Number( parseFloat(porcentaje_obligado).toFixed(0) );
              markPointColor = (Number(porcentaje_obligado) < Number(porcentaje_planeado)) ? "#ab2900" : "#099836";
           } 

    }

    var options = {
      series: [{
        name: "Proyección",
        type: "line",
        data: proyectadoPresidencia,
        itemStyle: {
          normal: {
            color: "#069169"
          }
        }
      },{
        name: "Ejecución",
        type: "line",
        data: ejecutado,
         markPoint : {
               data : [
                 { value: markPointValue + "%", xAxis: markPointMes, yAxis: markPointValue }
               ],
               itemStyle: {
                normal: {
                  color: markPointColor
                }
              }
        },
        itemStyle: {
          normal: {
            color: "#004185"
          }
        }
      }, {
            name:'Mejor Año',
            type:'line',
            smooth:true,
            itemStyle: {normal: { color: "#ddd", borderWidth : 9, areaStyle: {type: 'default', color : '#ddd'}}},
            data: mejorAnio
      }]
    };
    this.echart.updateData(options);
  }


  setError(error) {
    this.error = true;
    this.noData = false;
    this.loading = false;
    this.noSIIFLoader = false;
  }


 public getData(data) {
    this.error = false;
   this.noData = false;
   this.loading = true;
       this.noSIIFLoader = false;

    this.anio = data.anio;
    this.mes = data.mes;
      this.proyectoHttpService.getLiquidacionSectorPresidencia(this.anio)
          .subscribe(data => this.updateData(data),
          error => this.setError(error));
  }


  constructor(private proyectoHttpService: ProyectoHttpService, private sectorDataService : SectorDataService) {
  }


  ngOnInit() {
    (this.sectorDataService._data !== null) ? this.getData(this.sectorDataService._data) : null;
    this.sectorDataService.data.subscribe(data => {
      this.getData(data);
    });
  }

}
