import { TestBed, inject } from '@angular/core/testing';
import { CdpHttpService } from './cdp-http.service';

describe('LoginHttpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CdpHttpService]
    });
  });

  it('should ...', inject([CdpHttpService], (service: CdpHttpService) => {
    expect(service).toBeTruthy();
  }));
});
