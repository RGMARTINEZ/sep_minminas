/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { CsUploadComponent } from './cs-upload.component';
import { LoadingService } from '../../core/loading/loading.service';
import { MessageService } from '../../core/services/message.service';
import { StorageManagerService } from '../../core/services/storage-manager.service';



describe('Component: CsUpload', () => {
  it('should create an instance', () => {
    let loadingService: LoadingService;
    let messageService: MessageService;
    let storageManagerService: StorageManagerService;

    let component = new CsUploadComponent(loadingService, messageService, storageManagerService);
    expect(component).toBeTruthy();
  });
});
