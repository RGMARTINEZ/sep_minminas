import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';


@Injectable()
export class TopNavbarService {
 
  // Observable showMenu source
  private obsShowMenu = new BehaviorSubject<boolean>(false);
  // Observable showMenu stream
  showMenu = this.obsShowMenu.asObservable();
  // service command
  changeNav(boolean) {
    this.obsShowMenu.next(boolean);
  }
  
  constructor() {
   }

}
