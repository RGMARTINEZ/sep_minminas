import { Directive, ElementRef, Renderer, Input, AfterContentInit, ChangeDetectorRef } from '@angular/core';
import { TopNavbarService } from '../top-navbar/top-navbar.service';
import {Subscription} from 'rxjs/Subscription';
import * as echarts from 'echarts/dist/echarts';

@Directive({
  selector: '[app-echart-bar]'
})

export class EchartBarDirective {

  innerWidth: number;
  private myChart;
  private window: Window;
  subscription: Subscription;
  private listenerEcharts;


  @Input() echartOptions: string;

  constructor(private el: ElementRef, private renderer: Renderer, private cdr: ChangeDetectorRef, private topNavbarService: TopNavbarService) {



    window.onresize = () => {
      this.myChart.resize();
      this.cdr.detectChanges(); //running change detection manually
    };


    this.renderer.setElementStyle(this.el.nativeElement, 'width', "100%");
    this.renderer.setElementStyle(this.el.nativeElement, 'height', "100%");


    this.subscription = this.topNavbarService.showMenu.subscribe(
      showMenu => console.log("aqui resize")
    );


  }



  ngAfterContentInit() {

    console.log(this.echartOptions);

    this.myChart = echarts.init(this.el.nativeElement);
    this.myChart.setOption(this.echartOptions);
  }


}