import { Component, ViewChild, OnInit } from '@angular/core';
import { EchartsDirective } from '../../../core/directives/echarts.directive';
import { SeguimientoEjecucionDataService } from '../../seguimiento-ejecucion-data.service'; 
import { ProcesarObligaciones } from '../../model/ProcesarObligaciones';

@Component({
  selector: 'app-se-anh-obligaciones',
  templateUrl: './se-anh-obligaciones.component.html',
  styleUrls: ['./se-anh-obligaciones.component.less']
})
export class SeAnhObligacionesComponent extends ProcesarObligaciones implements OnInit {

  @ViewChild(EchartsDirective) echart: EchartsDirective;

  public echartOption: any = {};
  
  setDataToChart(options) {
     this.echart.updateData(options);
  }

  goToProjects() {
    this.sData.goToProject(this.UEJ);
  }

  
  constructor(private sData : SeguimientoEjecucionDataService) {
    super();
    this.UEJ = "21-11-00";
    this.sData.dataGraficos.subscribe(data => {
      this.updateData(data);
    });

        this.sData.dataDatos.subscribe(entidades => {
      this.getDataEntidad(entidades)
    })

  }


  ngOnInit() {
  }

}
