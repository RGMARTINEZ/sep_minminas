import { Component, OnInit } from '@angular/core';
import { PresupuestoHttpService } from './presupuesto-http.service';

@Component({
  selector: 'app-presupuesto',
  templateUrl: './presupuesto.component.html',
  styleUrls: ['./presupuesto.component.css'],
  providers: [PresupuestoHttpService]
})
export class PresupuestoComponent implements OnInit {

  public currentType: string = 'SECTOR';
  private dataSource: Array<any> = new Array<any>();
  public data: Array<any> = new Array<any>();
  private error: any;

  changeType() {
    switch (this.currentType) {
      case 'SECTOR':
        this.data = this.dataSource;
        break;
      case 'SEC_GENERAL':
        this.filterDataByGrupo(this.currentType);
        break;
      case 'SUBSIDIOS':
        this.filterDataByType('subsidios');
        break;
      case 'FONDOS':
        this.filterDataByType('fondos');
        break;
      case 'VICE_MINAS':
        this.filterDataByGrupo(this.currentType);
        break;
      case 'VICE_ENERGIA':
        this.filterDataByGrupo(this.currentType);
        break;
      default:
        break;
    }
  }

  constructor(private presupuestoHttpService: PresupuestoHttpService) { }

  ngOnInit() {
    this.presupuestoHttpService.getPresupuesto().subscribe(data => this.getData(data), error => this.error = <any>error);
  }

  private filterDataByGrupo(grupo: string) {
    this.data = [];
    for (let i = 0; i < this.dataSource.length; i++) {
      if (this.dataSource[i].responsable === grupo) {
        this.data.push(this.dataSource[i]);
      }
    }
  }

  private filterDataByType(type: string) {
    this.data = [];
    for (let i = 0; i < this.dataSource.length; i++) {
      if (this.dataSource[i][type] === true) {
        this.data.push(this.dataSource[i]);
      }
    }


  }


  private getData(data: any) {
    /*
    const data = [{
      "idPresupuesto": 207,
      "bpin": 2015011000191,
      "nombreProyecto": "IMPLEMENTACIÓN DE LOS SERVICIOS DE INFORMACIÓN Y COMUNICACIÓN INSTITUCIONAL DEL MINISTERIO DE MINAS Y ENERGÍA-",
      "vigenciaAnterior": 2440,
      "vigenciaActual": 4000,
      "cuotaAprobada": 4000,
      "cuotaSolicitada": 4243,
      "anio": 2017,
      "dependencia": "Despacho Ministro",
      "grupo": "SEC_GENERAL",
      "fondos": false,
      "subsidios": true,
      "sector": "MME"
    },{
      "idPresupuesto": 208,
      "bpin": 2015011000349,
      "nombreProyecto": "FORTALECIMIENTO AL ACCESO Y TRANSPARENCIA DE LA INFORMACION EXTRACTIVA - EITI NACIONAL",
      "vigenciaAnterior": 990,
      "vigenciaActual": 1915,
      "cuotaAprobada": 1486,
      "cuotaSolicitada": 1486,
      "anio": 2017,
      "dependencia": "Vice Minas",
      "grupo": "SEC_GENERAL",
      "fondos": false,
      "subsidios": true,
      "sector": "MME"
    },{
      "idPresupuesto": 209,
      "bpin": 2014011000201,
      "nombreProyecto": "FORTALECIMIENTO INSTITUCIONAL DEL SECTOR MINERO ENERGÉTICO NACIONAL",
      "vigenciaAnterior": 300,
      "vigenciaActual": 8214,
      "cuotaAprobada": 21536,
      "cuotaSolicitada": 21537,
      "anio": 2017,
      "dependencia": "Vice Minas",
      "grupo": "VICE_MINAS",
      "fondos": true,
      "subsidios": false,
      "sector": "MME"
    },{
      "idPresupuesto": 210,
      "bpin": 2012011000095,
      "nombreProyecto": "CONTROL A LA EXPLOTACIÓN ILÍCITA DE MINERALES COLOMBIA -",
      "vigenciaAnterior": 1362,
      "vigenciaActual": 5000,
      "cuotaAprobada": 5000,
      "cuotaSolicitada": 8000,
      "anio": 2017,
      "dependencia": "Vice Minas",
      "grupo": "VICE_MINAS",
      "fondos": true,
      "subsidios": false,
      "sector": "MME"
    },{
      "idPresupuesto": 211,
      "bpin": 2016011000195,
      "nombreProyecto": "FORTALECIMIENTO DE LA GESTIÓN INSTITUCIONAL EN TERRITORIO Y CONTRIBUCIÓN A LA GOBERNABILIDAD DEL SECTOR MINERO ENERGÉTICO NACIONAL",
      "vigenciaAnterior": 2911,
      "vigenciaActual": 3500,
      "cuotaAprobada": 5000,
      "cuotaSolicitada": 10000,
      "anio": 2017,
      "dependencia": "Ambiental",
      "grupo": "VICE_ENERGIA",
      "fondos": true,
      "subsidios": false,
      "sector": "MME"
    },{
      "idPresupuesto": 212,
      "bpin": 23001140000,
      "nombreProyecto": "DIAGNOSTICO MINERO AMBIENTAL DE LOS PASIVOS EN EL TERRITORIO NACIONAL",
      "vigenciaAnterior": 1296,
      "vigenciaActual": 1080,
      "cuotaAprobada": 1500,
      "cuotaSolicitada": 7000,
      "anio": 2017,
      "dependencia": "Ambiental",
      "grupo": "VICE_ENERGIA",
      "fondos": false,
      "subsidios": false,
      "sector": "MME"
    }];
    */
      for (let i = 0; i < data.length; i++) {
        data[i]['total'] = data[i].vigenteanterior +
        data[i].vigenteactual +
        data[i].solicitado +
        data[i].cuotaaprobada;
      };
      this.dataSource = data;
      this.changeType();
  }





}
