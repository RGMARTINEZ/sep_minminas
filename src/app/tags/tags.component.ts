import { Component, OnInit } from '@angular/core';
import { TagsHttpService } from './tags-http.service';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.less'],
  providers : [TagsHttpService]
})
export class TagsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
