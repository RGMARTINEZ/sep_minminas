import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumenCifrasComponent } from './resumen-cifras.component';

describe('ResumenCifrasComponent', () => {
  let component: ResumenCifrasComponent;
  let fixture: ComponentFixture<ResumenCifrasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumenCifrasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumenCifrasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
