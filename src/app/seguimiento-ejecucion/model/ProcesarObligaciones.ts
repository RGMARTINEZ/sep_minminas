export class ProcesarObligaciones {
    UEJ : string;
    currentMes : number;
    tipoPresupuesto : string;

    entidad : any = {
    apropiacion_disponible : 0,
    comprometido : 0,
    compromiso : 0,
    obligacion : 0,
    por_comprometer : 0,
    por_obligar_de_los_compromiso : 0,
    por_obligar_si_se_compromete_el_100 : 0,
    comprometido_apropiacion : 0,
    compromiso_apropiacion : 0,
    obligado_apropiacion : 0,
    obligacion_apropiacion : 0
  };


  showData : boolean = false;

  showDatos() {
    this.showData = !this.showData;
  }


 public updateData(data) {
    let planeado = [];
    let ejecutado = [];
    let markPointMes = data[0].mes - 1;
    let markPointValue = 0;
    let markPointColor;
    for (var i = 0; i < data.length ; i++) {
      if (data[i].uej == this.UEJ) {
        var obligaciones = data[i].obligaciones;
        for (var j = 0; j < obligaciones.length ; j++) {
         var porcentaje_ejecutado = null;
         var porcentaje_planeado = null;
/*
         (obligaciones[j].planeado != 0) ? porcentaje_planeado = parseFloat((obligaciones[j].planeado / data[i].apropiacionVigente[obligaciones.length -1].valor) * 100 + "").toFixed(2) : null;
         (obligaciones[j].ejecutado != 0) ? porcentaje_ejecutado = parseFloat( (obligaciones[j].ejecutado / data[i].apropiacionVigente[obligaciones.length -1].valor) * 100 + "").toFixed(2) : null;
*/
         (obligaciones[j].planeado != 0) ? porcentaje_planeado = parseFloat((obligaciones[j].planeado / data[i].apropiacionVigente[j].valor) * 100 + "").toFixed(2) : null;
         (obligaciones[j].ejecutado != 0) ? porcentaje_ejecutado = parseFloat( (obligaciones[j].ejecutado / data[i].apropiacionVigente[j].valor) * 100 + "").toFixed(2) : null;

         planeado.push(porcentaje_planeado);
           ejecutado.push(porcentaje_ejecutado);
           if (j == markPointMes) {
              markPointValue = Number( parseFloat(porcentaje_ejecutado).toFixed(0) );
              markPointColor = (Number(porcentaje_ejecutado) < Number(porcentaje_planeado)) ? "#ab2900" : "#099836";
           } 
        }
        break;
      }
    }

    var options = {
      series: [{
        name: "Proyección",
        type: "line",
        data: planeado,
        itemStyle: {
          normal: {
            color: "#069169"
          }
        }
      },{
        name: "Ejecución",
        type: "line",
        data: ejecutado,
         markPoint : {
               data : [
                 { value: markPointValue, xAxis: markPointMes, yAxis: markPointValue }
               ],
              itemStyle: {
                normal: {
                  color: markPointColor
                }
              }
           },
        itemStyle: {
          normal: {
            color: "#004185"
          }
        }
      }]
    };
    this.setDataToChart(options);
  }



  getDataEntidad(params) {
    this.currentMes = params.mes;
    this.tipoPresupuesto = params.tipoPresupuesto;
    var entidades  = params.entidades;

    for (var i = 0; i < entidades.length ; i++) {
      if (entidades[i].uej == this.UEJ) {
        this.entidad = entidades[i];
        this.entidad.comprometido_apropiacion = this.entidad.comprometido / this.entidad.apropiacion_disponible * 100;
        this.entidad.compromiso_apropiacion = this.entidad.compromiso / this.entidad.apropiacion_disponible * 100;
        this.entidad.obligado_apropiacion = this.entidad.obligado / this.entidad.apropiacion_disponible * 100;
        this.entidad.obligacion_apropiacion = this.entidad.obligacion / this.entidad.apropiacion_disponible * 100;   
        break;
      }
    }
  }
  
      setDataToChart(options) {
    }


}